// AkimaInterpolator1D.cpp: implementation of the AkimaInterpolator1D class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "AkimaInterpolator1D.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{

///Default constructor
AkimaInterpolator1D::AkimaInterpolator1D()
{

}

///Destructor
AkimaInterpolator1D::~AkimaInterpolator1D()
{
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
}


///This constructor requires 3 parameters :
///		- the name of the interpolator
///		- the description of the interpolator
///		- the interpolated data
AkimaInterpolator1D::AkimaInterpolator1D(	
				std::string sName, 
				std::string sDescription, 
				InterpolationData1D* mInterpolationData) : 
				Interpolator1D(sName,sDescription,"Akima",
				mInterpolationData)

{
	acc		= gsl_interp_accel_alloc ();
	spline	= gsl_spline_alloc (gsl_interp_akima, getNbData());
	gsl_spline_init (spline, mInterpolationData->getXValues(), mInterpolationData->getYValues(), getNbData());
}

///Return the interpolated value for the wanted dValue
double AkimaInterpolator1D::getInterpolatedValue(double dValue)
{
      return gsl_spline_eval (spline, dValue, acc);
}

///Method to update the interpolator according the new InterpolationData values
void AkimaInterpolator1D::updateInterpolator()
{
	gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
	acc		= gsl_interp_accel_alloc ();
	spline	= gsl_spline_alloc (gsl_interp_akima, getNbData());
	gsl_spline_init (spline, getInterpolatedData()->getXValues(), getInterpolatedData()->getYValues(), getNbData());
}

}

