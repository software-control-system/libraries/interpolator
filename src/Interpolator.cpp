// Interpolator.cpp: implementation of the Interpolator class.
//
//////////////////////////////////////////////////////////////////////

#include "Interpolator.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
	bool Interpolator::_verb = false;
///Default constructor
Interpolator::Interpolator()
{

}

///Destructor
Interpolator::~Interpolator()
{

}

///This constructor requires 3 parameters :
///		- the name of the interpolator
///		- the description of the interpolator
///		- the interpolated type
Interpolator::Interpolator(	std::string sName, 
							std::string sDescription, 
							std::string sInterpolationType) : 
_sName(sName),_sDescription(sDescription),_sInterpolationType(sInterpolationType)
{
}


///Return the name of the interpolator
std::string	Interpolator::getName() const
{
	return _sName;
}
///Return the description of the interpolator	
std::string	Interpolator::getDescription() const
{
	return _sDescription;
}

///Return the type of the interpolator	
std::string	Interpolator::getInterpolationType() const
{
	return _sInterpolationType;
}
}

