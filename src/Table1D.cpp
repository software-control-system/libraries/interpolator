// Table1D.cpp: implementation of the Table1D class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "Table1D.h"
//#include "Monochromator.h"
#include "LinearInterpolator1D.h"
#include "AkimaInterpolator1D.h"
#include "PeriodicAkimaInterpolator1D.h"
#include "CubicSplineInterpolator1D.h"
#include "PeriodicCubicSplineInterpolator1D.h"
#include "PolynomialInterpolator1D.h"


namespace Interpolator
{

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

///Default constructor
/*Table1D::Table1D()
{

}*/

///Destructor
Table1D::~Table1D()
{	
	DESTRUCTION(_mInterpolator);
	DESTRUCTION(_mData);
}

///This constructor requires 6 parameters :
///		- the name of the Table1D
///		- the description of the Table1D
///		- the entry point name (must follow the naming convention for the Movements/Variables)
///		- the output point name (must follow the naming convention for the Movements/Variables)
///		- the entry point variable
///		- the direct interpolation object
///		- the inverse interpolation object (USELESS !!!!)
///\todo MODIFIER LA CLASSE INTERPOLATOR pour permettre d'interpoler dans les 2 sens
Table1D::Table1D(	std::string			sName,
					std::string			sDescription,	
					std::string			sEntryPoint, 
					std::string			sOutputPoint,
					Variable*			mEntryPoint,
					Interpolator1D*		mInterpolator/*,
					Interpolator*	mInverseInterpolator*/) : 
Table(sName,sDescription,""),
_sEntryPoint(sEntryPoint),_sOutputPoint(sOutputPoint),
_mEntryPoint(mEntryPoint),_mInterpolator(mInterpolator)
{
	///_sEntryPoint  peut �tre initialis� � partir de l'interpolateur->interpoaltionData->getXname()
}


///This constructor requires 9 parameters :
///		- the name of the Table1D
///		- the description of the Table1D
///		- the entry point name (must follow the naming convention for the Movements/Variables)
///		- the output point name (must follow the naming convention for the Movements/Variables)
///		- the entry point variable
///		- the wanted interpolation type ("Akima","Periodic Akima","Linear","Cubic Spline","Periodic Cubic Spline","Polynomial")
///		- the number of Data
///		- the X values
///		- the Y values
///If the interpolation type name entered is invalid the interpolator object is set to NULL
///\todo MODIFIER LA CLASSE INTERPOLATOR pour permettre d'interpoler dans les 2 sens
Table1D::Table1D(	std::string			sName,
					std::string			sDescription,	
					std::string			sEntryPoint, 
					std::string			sOutputPoint,
					Variable*			mEntryPoint,
					std::string			sInterpolationType,
					long				lNbData,
					double*				mX,
					double*				mY) :
Table(sName,sDescription,sInterpolationType),
_sEntryPoint(sEntryPoint),_sOutputPoint(sOutputPoint),_mEntryPoint(mEntryPoint)

{
_mData			= new InterpolationData1D(sEntryPoint,sOutputPoint,lNbData,mX,mY);
	

if (sInterpolationType == "Akima") 
{
	std::string sInterpolationName = sEntryPoint;
	sInterpolationName+= "-->";
	sInterpolationName+= sOutputPoint;
	_mInterpolator = new AkimaInterpolator1D(sDescription,sInterpolationName,_mData);
}

else if (sInterpolationType == "Periodic Akima") 
{
	std::string sInterpolationName = sEntryPoint;
	sInterpolationName+= "-->";
	sInterpolationName+= sOutputPoint;
	_mInterpolator = new PeriodicAkimaInterpolator1D(sDescription,sInterpolationName,_mData);
}

///\todo FAIRE les differents cas d'interpolateurs
else if (sInterpolationType == "Linear") 
{
	std::string sInterpolationName = sEntryPoint;
	sInterpolationName+= "-->";
	sInterpolationName+= sOutputPoint;
	_mInterpolator = new LinearInterpolator1D(sDescription,sInterpolationName,_mData);
}

else if (sInterpolationType == "Cubic Spline") 
{
	std::string sInterpolationName = sEntryPoint;
	sInterpolationName+= "-->";
	sInterpolationName+= sOutputPoint;
	_mInterpolator = new CubicSplineInterpolator1D(sDescription,sInterpolationName,_mData);
}

else if (sInterpolationType == "Periodic Cubic Spline") 
{
	std::string sInterpolationName = sEntryPoint;
	sInterpolationName+= "-->";
	sInterpolationName+= sOutputPoint;
	_mInterpolator = new PeriodicCubicSplineInterpolator1D(sDescription,sInterpolationName,_mData);
}

else if (sInterpolationType == "Polynomial") 
{
	std::string sInterpolationName = sEntryPoint;
	sInterpolationName+= "-->";
	sInterpolationName+= sOutputPoint;
	_mInterpolator = new PolynomialInterpolator1D(sDescription,sInterpolationName,_mData);
}

else //Error
{
	_mInterpolator = 0;

}

}

///This constructor requires 6 parameters :
///		- the name of the Table1D
///		- the description of the Table1D
///		- the wanted interpolation type ("Akima","Periodic Akima","Linear","Cubic Spline","Periodic Cubic Spline","Polynomial")
///		- the file path for the data
///		- the column to used in the GenericFileReader1D for the X column (first column = entry point)
///		- the column to used in the GenericFileReader1D for the Y column (second column = output point)
///If the interpolation type name entered is invalid the interpolator object is set to NULL
Table1D::Table1D(	std::string			sName,
					std::string			sDescription,	
					std::string			sInterpolationType,
					std::string			sFilePath,
					int					iFirstColumn,
					int					iSecondColumn) :
Table(sName,sDescription,sInterpolationType,sFilePath)
{
	GenericFileReader1D* _mFileReader = new GenericFileReader1D(sFilePath);
	
	double* mX = _mFileReader->getColumn(iFirstColumn);
	double* mY = _mFileReader->getColumn(iSecondColumn);

	_sEntryPoint = _mFileReader->getColumnName(iFirstColumn);
	_sOutputPoint = _mFileReader->getColumnName(iSecondColumn);
	
	_mEntryPoint = (Variable*)0; //Or mMono->getVariable(_sEntryPoint);

	long lNbData = _mFileReader->getNbData();

	_mData			= new InterpolationData1D(_sEntryPoint,_sOutputPoint,lNbData,mX,mY);
	
	if (sInterpolationType == "Akima") 
	{
		std::string sInterpolationName = _sEntryPoint;
		sInterpolationName+= "-->";
		sInterpolationName+= _sOutputPoint;
		_mInterpolator = new AkimaInterpolator1D(sDescription,sInterpolationName,_mData);
	}

	else if (sInterpolationType == "Periodic Akima") 
	{
		std::string sInterpolationName = _sEntryPoint;
		sInterpolationName+= "-->";
		sInterpolationName+= _sOutputPoint;
		_mInterpolator = new PeriodicAkimaInterpolator1D(sDescription,sInterpolationName,_mData);
	}

	///\todo FAIRE les differents cas d'interpolateurs
	else if (sInterpolationType == "Linear") 
	{
		std::string sInterpolationName = _sEntryPoint;
		sInterpolationName+= "-->";
		sInterpolationName+= _sOutputPoint;
		_mInterpolator = new LinearInterpolator1D(sDescription,sInterpolationName,_mData);
	}

	else if (sInterpolationType == "Cubic Spline") 
	{
		std::string sInterpolationName = _sEntryPoint;
		sInterpolationName+= "-->";
		sInterpolationName+= _sOutputPoint;
		_mInterpolator = new CubicSplineInterpolator1D(sDescription,sInterpolationName,_mData);
	}

	else if (sInterpolationType == "Periodic Cubic Spline") 
	{
		std::string sInterpolationName = _sEntryPoint;
		sInterpolationName+= "-->";
		sInterpolationName+= _sOutputPoint;
		_mInterpolator = new PeriodicCubicSplineInterpolator1D(sDescription,sInterpolationName,_mData);
	}

	else if (sInterpolationType == "Polynomial") 
	{
		std::string sInterpolationName = _sEntryPoint;
		sInterpolationName+= "-->";
		sInterpolationName+= _sOutputPoint;
		_mInterpolator = new PolynomialInterpolator1D(sDescription,sInterpolationName,_mData);
	}

	else //Error
	{
		_mInterpolator = 0;

	}

	delete (_mFileReader);
}

///Return the output value according the entry point value dValue parameter
double Table1D::computeValue(double dValue)
{
	return _mInterpolator->getInterpolatedValue(dValue);
}

///Compute the value (use the singleton monochromator to get all the data according the entry point)
double Table1D::computeValue()
{
	/*/------------ !!!!! ATTENTION: F. LANGLOIS:
	La partie ci dessous est comment�e pour permettre a la lib Interpolator de ne plus faire reference a la librairie Monochromator
	Ceci peut avoir des consequences sur les devices deja existants.
	a ma connaissance, aucun device a ce jour n'utilise cette methode
	*/


	/*Monochromator* mMonochromator	= Monochromator::GetMonochromatorInstance();
	//std::string sEntryPointCorrection	= _mInterpolator->getInterpolatedData()->getXName();
 	Variable* mVariable				= mMonochromator->getVariable(_sEntryPoint);
	double dValue					= mVariable->getValue(); 
	return _mInterpolator->getInterpolatedValue(dValue);*/

	return 0.0;
}

///Return the entry point	
std::string Table1D::getEntryPoint() const
{
	return _sEntryPoint;
}

///Return the output point	
std::string Table1D::getOutputPoint() const
{
	return _sOutputPoint;
}
	
///Return the interpolation data object
InterpolationData1D* Table1D::getInterpolationData() const
{
	return _mData;
}

///Return the interpolatordata object
Interpolator1D* Table1D::getInterpolator() const
{
	return _mInterpolator;
}

///Display the table 1D informations\n Used mainly for DEBUG
void Table1D::printInfos()
{
  	if(Interpolator::_verb == true) cout << "\n############ Table1D : " << getName() << " ############\n" << endl;
	if(Interpolator::_verb == true) cout << "Interpolation Type   : " << getInterpolationType() << endl;
	if(Interpolator::_verb == true) cout << "Description          : " << getDescription() << endl;
	if(Interpolator::_verb == true) cout << "Columns Names        : " << getEntryPoint() << " --> " << getOutputPoint() << endl;
	if(Interpolator::_verb == true) cout << "Values\n" << endl;
	_mData->DisplayData();
}



///Method to modify the ith value of the X interpolation data structure with the dNewValue
void Table1D::setXValue(int i,double dNewValue)
{
	//First we update the interpolation data structure values
	_mInterpolator->getInterpolatedData()->setXValue(i,dNewValue);
	//And after update of the gsl object according the new InterpolationData values
	_mInterpolator->updateInterpolator();
}

///Method to modify the ith value of the Y interpolation data structure with the dNewValue
void Table1D::setYValue(int i,double dNewValue)
{
	//First we update the interpolation data structure values
	_mInterpolator->getInterpolatedData()->setYValue(i,dNewValue);
	//And after update of the gsl object according the new InterpolationData values
	_mInterpolator->updateInterpolator();
}

///Method to modify the values of the interpolation data structure with the dNewXValues and dNewYValues
void Table1D::setValues(long lNbData,double* dNewXValues,double* dNewYValues)
{
	//First we update the interpolation data structure values
	_mInterpolator->getInterpolatedData()->setValues(lNbData,dNewXValues,dNewYValues);
	//And after update of the gsl object according the new InterpolationData values
	_mInterpolator->updateInterpolator();
}



///Return the ith value of X array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the X array is null	
double Table1D::getXValue(int i) const
{
	return _mInterpolator->getInterpolatedData()->getXValue(i);
}
	
///Return the ith value of Y array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the Y array is null	
double Table1D::getYValue(int i) const
{
	return _mInterpolator->getInterpolatedData()->getYValue(i);
}

///Return the X values (first column)
///\exception This method throw a NullPointerException exception if the X array is null		
double* Table1D::getXValues() const
{
	return _mInterpolator->getInterpolatedData()->getXValues();
}

///Return the Y values (second column)
///\exception This method throw a NullPointerException exception if the Y array is null	
double* Table1D::getYValues() const
{
	return _mInterpolator->getInterpolatedData()->getYValues();
}

///Return the number of data
long Table1D::getNbData() const
{
	return _mInterpolator->getInterpolatedData()->getNbData();
}





}


