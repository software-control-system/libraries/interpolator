// GenericFileReader1D1D.cpp: implementation of the GenericFileReader1D class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "GenericFileReader1D.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
GenericFileReader1D::GenericFileReader1D()
{
	_mColumnsVector		= new vector< double* >();
	_mColumnsNameVector = new vector< std::string >();
}

///Destructor
GenericFileReader1D::~GenericFileReader1D()
{
  for (int i=0;i< (signed)_mColumnsVector->size();i++) 
	{
		double * pColumn = getColumn(i);
		if (pColumn) 
		{
			delete [] pColumn;
			pColumn = 0;
		}
	}

	DESTRUCTION (_mColumnsVector);	
	DESTRUCTION (_mColumnsNameVector);
}

///This constructor requires 1 parameter :
///		- the name of the file to read (the file must provide a header giving the number of variables and number of lines)
GenericFileReader1D::GenericFileReader1D(const std::string& sFileName) : FileReader(sFileName) 
{
	_mColumnsVector = new vector< double* >();
	_mColumnsNameVector = new vector< std::string >();
	Parse();
}

///This constructor requires 3 parameters :
///		- the name of the file to read
///		- the number of columns of the file
///		- the number of lines of the file (without the first line which contains the name of the data)	
GenericFileReader1D::GenericFileReader1D(const std::string& sFileName,long lNbColumns,long lNbLines) : FileReader(sFileName,lNbColumns,lNbLines)
{
	_mColumnsVector = new vector< double* >();
	_mColumnsNameVector = new vector< std::string >();
	Parse();
}


///This method parse the buffer to read a generic file\n
void GenericFileReader1D::Parse() // throw (ParseException)
{

int iNbData = getNbData();
if(Interpolator::_verb == true) cout << "NbData : " << iNbData << endl;

int iNbVariables = getNbVariables();
if(Interpolator::_verb == true) cout << "NbVariables : " << iNbVariables << endl;

//Reading of the first line to extract the variables names
std::string sColumnName;

int i=0;

for (i=0;i<iNbVariables;i++) 
{
	sColumnName = extractString();
	_mColumnsNameVector->push_back(sColumnName);
	if(Interpolator::_verb == true) cout << "Column " << i << " --> " << 	(*_mColumnsNameVector)[i] << endl;

	_mColumnsVector->push_back(new double[iNbData]);

}

double dValue;

for (i=0;i<iNbData;i++) 
{
	for (int j=0;j<iNbVariables;j++) 
	{
		dValue = extractDouble();
			
		((*_mColumnsVector)[j])[i] = dValue;

	}
	
}
displayAll();

}


///Return the iColumnNumber column data 
///\exception This method throw a IndexOutOfBoundException exception if the iColumnNumber is outside the bound
///\exception This method throw a NullPointerException exception if the data vector is null	
double* GenericFileReader1D::getColumn(int iColumnNumber)	const	// throw (IndexOutOfBoundException,NullPointerException)
{
  if ((iColumnNumber < 0) || (iColumnNumber >= (int)_mColumnsVector->size())) throw IndexOutOfBoundException("iColumnNumber",iColumnNumber,0,getNbData(),"GenericFileReader1D::getColumn(int iColumnNumber)",__FILE__,__LINE__); 

	if (_mColumnsVector == 0) throw NullPointerException("_mColumnsVector","GenericFileReader1D::getColumn(int iColumnNumber)",__FILE__,__LINE__);
	return ((*_mColumnsVector)[iColumnNumber]);
}

///Return the iColumnNumber column data name
///\exception This method throw a IndexOutOfBoundException exception if the iColumnNumber is outside the bound
///\exception This method throw a NullPointerException exception if the name vector is null
std::string GenericFileReader1D::getColumnName(int iColumnNumber)	const	// throw (IndexOutOfBoundException,NullPointerException)
{
  if ((iColumnNumber < 0) || (iColumnNumber >= (int)_mColumnsNameVector->size())) throw IndexOutOfBoundException("iColumnNumber",iColumnNumber,0,getNbData(),"GenericFileReader1D::getColumnName(int iColumnNumber)",__FILE__,__LINE__); 

	if (_mColumnsNameVector == 0) throw NullPointerException("_mColumnsNameVector","GenericFileReader1D::getColumnName(int iColumnNumber)",__FILE__,__LINE__);
	return ((*_mColumnsNameVector)[iColumnNumber]);
}
	
///Return the value of iLineNumber line and iColumnNumber column
///\exception This method throw a IndexOutOfBoundException exception if the iColumnNumber or the iLineNumber are outside the bound
///\exception This method throw a NullPointerException exception if the data vector is null
double GenericFileReader1D::getValue(int iColumnNumber,int iLineNumber)	const	// throw (IndexOutOfBoundException,NullPointerException)
{
	if ((iColumnNumber < 0) || (iColumnNumber >= (int)_mColumnsVector->size())) throw IndexOutOfBoundException("iColumnNumber",iColumnNumber,0,getNbVariables(),"GenericFileReader1D::getValue(int iColumnNumber,int jLineNumber)",__FILE__,__LINE__); 
	if ((iLineNumber < 0) || (iLineNumber >= getNbData())) throw IndexOutOfBoundException("iLineNumber",iLineNumber,0,getNbData(),"GenericFileReader1D::getValue(int iColumnNumber,int jLineNumber)",__FILE__,__LINE__); 
	
	if (_mColumnsVector == 0) throw NullPointerException("_mColumnsVector","GenericFileReader1D::getValue(int iColumnNumber,int jLineNumber)",__FILE__,__LINE__);
	
	return ((*_mColumnsVector)[iColumnNumber])[iLineNumber];
}

///Display the data\n Used for debug
/*void GenericFileReader1D::displayAll()
{
	for (int i=0;i<getNbVariables();i++)
	{
		if(Interpolator::_verb == true) cout << "Column : " << i << " --> " << (*_mColumnsNameVector)[i] << endl;
		for (int j=0;j<getNbData();j++)
		{
			if(Interpolator::_verb == true) cout << "[" << j << "=" << getValue(i,j) << "]" << endl;
		}
	}
	
}
*/
///Display the data\n Used for debug
void GenericFileReader1D::displayAll()
{
	for (int i=0;i<getNbVariables();i++)
	{
		if(Interpolator::_verb == true) cout << "Column : " << i << " --> " << (*_mColumnsNameVector)[i] << endl;		
	}

	for (int j=0;j<getNbData();j++)
	{
		if(Interpolator::_verb == true) cout	<< "[" << j << "=\t";
		for (int i=0;i<getNbVariables();i++)
		{
			if(Interpolator::_verb == true) cout << getValue(i,j) << "\t";		
		}
		if(Interpolator::_verb == true) cout << "]" << endl;
	}
}






}

