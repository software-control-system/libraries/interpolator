#include "DataFile.h"

namespace ICLIB {

const std::vector< double >& DataFile::getValues(const std::string& key) const /*throw(Tango::DevFailed)*/ {
	ValuesMap::const_iterator it;
	for(it=mValues.begin(); it!=mValues.end() && it->first!=key; ++it);
	if(it==mValues.end()) {
		std::ostringstream err;
		err << "Could not find data for '" << key << "' in file '" << mFilename << "'";
		throw ParseException (	mFilename,
								"DataFile::getValues",
								__FILE__,
								__LINE__);
					
								
		/*Tango::Except::throw_exception( 
			static_cast<const char*>("TANGO_DEVICE_ERROR"), 
			static_cast<const char*>(err.str().c_str()),
			static_cast<const char*>("DataFile::getValues"));*/
	}
	else
		return it->second;
}

void DataFile::setValues(const std::string& key,const std::vector< double >& data) /*throw(Tango::DevFailed) */{

	ValuesMap::iterator it;
	for(it=mValues.begin(); it!=mValues.end() && it->first!=key; ++it);
	if(it==mValues.end()) {
		ValuesMap::value_type empty;
		mValues.push_back(empty);
		mValues.back().first=key;
		mValues.back().second=data;
	}
	else
		it->second=data;
}


} // namespace
