// SambaFileReader.cpp: implementation of the SambaFileReader class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "SambaFileReader.h"
#include <stdlib.h>
#include <vector>
#include <stdio.h>
using namespace std;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
SambaFileReader::SambaFileReader()
{

}

///Destructor
SambaFileReader::~SambaFileReader()
{

	if (_mThetaEncoder) 
	{
		delete [] _mThetaEncoder;
		_mThetaEncoder = 0;
	}
	
	if (_mThetaReal) 
	{
		delete [] _mThetaReal;
		_mThetaReal = 0;
	}
	
	if (_mThetaDiff) 
	{
		delete [] _mThetaDiff;
		_mThetaDiff = 0;
	}

}

///This constructor requires 1 parameter :
///		- the name of the file to read
SambaFileReader::SambaFileReader(const std::string& sFileName) : FileReader(sFileName) 
{
	Parse();
}

///This constructor requires 3 parameters :
///		- the name of the file to read
///		- the number of columns of the file
///		- the number of lines of the file (without the first line which contains the name of the data)
SambaFileReader::SambaFileReader(const std::string& sFileName,long lNbColumns,long lNbLines) : FileReader(sFileName,lNbColumns,lNbLines)
{
	Parse();
}

///This method parse the buffer to read a Samba file\n
///In this case only the 2 and 5 columns are extracted\n
///The column 2 is the theta encoder value variable column\n
///The column 5 is the theta real value variable column
void SambaFileReader::Parse() // throw (ParseException)
{

int iNbData = getNbData();
if(Interpolator::_verb == true) cout << "NbData : " << iNbData << endl;

int iNbVariables = getNbVariables();
if(Interpolator::_verb == true) cout << "NbVariables : " << iNbVariables << endl;

//Reading of the first line to extract the variables names
std::string sColumn1Name;

 int i=0;

for (i=0;i<iNbVariables;i++) 
{
	sColumn1Name = extractString();
	if(Interpolator::_verb == true) cout << "Column " << i << " --> " << sColumn1Name << endl;
}

_mThetaEncoder	= new double[iNbData];
_mThetaReal		= new double[iNbData];
_mThetaDiff		= new double[iNbData];
double dValue;

///We convert the dValue given in degre in the file into radians in order to work properly with the library
for (i=0;i<iNbData;i++) 
{
	for (int j=0;j<iNbVariables;j++) 
	{
		dValue = extractDouble();
		
		if (j==1) 
		{
			_mThetaEncoder[i]	= DegresToRadians(dValue);
		}
		if (j==4) 
		{
			_mThetaReal[i]		= DegresToRadians(dValue);	
		}
		if (j==5) 
		{	
			_mThetaDiff[i]		= DegresToRadians(dValue);	
		}
	}
	
}

if(Interpolator::_verb == true) cout << "_mThetaEncoder" <<endl;
for (int k=0;k<iNbData;k++)
{
	if(Interpolator::_verb == true) cout.precision(10);
	if(Interpolator::_verb == true) cout << "k=" << k << " --> " << _mThetaEncoder[k] << endl;
}

if(Interpolator::_verb == true) cout << "_mThetaReal" <<endl;
for (int l=0;l<iNbData;l++)
{ 
	if(Interpolator::_verb == true) cout.precision(10);
	if(Interpolator::_verb == true) cout <<  "l=" << l << " --> " << _mThetaReal[l] << endl;
}

if(Interpolator::_verb == true) cout << "ThetaEncoder-ThetaReal" <<endl;
for (int m=0;m<iNbData;m++)
{
	if(Interpolator::_verb == true) cout.precision(10);
	if(Interpolator::_verb == true) cout << "m=" << m << " --> " << _mThetaDiff[m] << endl;
}
	
}

///Return the encoder theta values array.
///\exception This method throw a NullPointerException exception if the slot vector is null
double* SambaFileReader::getThetaEncoder() const // throw (NullPointerException)
{
	if (_mThetaEncoder == 0) throw NullPointerException("_mThetaEncoder","SambaFileReader::getThetaEncoder()",__FILE__,__LINE__);
	return _mThetaEncoder;
}

///Return the real theta values array.
double* SambaFileReader::getThetaReal() const // throw (NullPointerException)
{
	if (_mThetaReal == 0) throw NullPointerException("_mThetaEncoder","SambaFileReader::getThetaEncoder()",__FILE__,__LINE__);
	return _mThetaReal;
}

///Return the (theta encoder - theta real) values array.
double* SambaFileReader::getThetaDiff() const // throw (NullPointerException)
{
	if (_mThetaDiff == 0) throw NullPointerException("_mThetaDiff","SambaFileReader::getThetaDiff()",__FILE__,__LINE__);
	return _mThetaDiff;
}

///Return the ith value of the X part of the array (in this case Theta Encoder values)
///\exception This method returns a IndexOutOfBoundException if the value with the ith index is not found.
double SambaFileReader::getXValue(int i) const // throw (IndexOutOfBoundException)
{
	if ((0<=i) && (i<getNbData())) 
	{
		return _mThetaEncoder[i];
	}
	else throw IndexOutOfBoundException("i",i,0,getNbData(),"SambaFileReader::getXValue(int i)",__FILE__,__LINE__);
}

///Return the ith value of the Y part of the array (in this case Theta Real values)
///\exception This method returns a IndexOutOfBoundException if the value with the ith index is not found.	
double SambaFileReader::getYValue(int i) const // throw (IndexOutOfBoundException)
{
	
	if ((0<=i) && (i<getNbData())) 
	{
		return _mThetaReal[i];
	}
	else throw IndexOutOfBoundException("i",i,0,getNbData(),"SambaFileReader::getYValue(int i)",__FILE__,__LINE__);

}

///Return the ith value of Theta Encoder array
///\exception This method returns a IndexOutOfBoundException if the value with the ith index is not found.
double SambaFileReader::getThetaEncoderValue(int i) const // throw (IndexOutOfBoundException)
{

	if ((0<=i) && (i<getNbData())) 
	{
		return getXValue(i);
	}
	else throw IndexOutOfBoundException("i",i,0,getNbData(),"SambaFileReader::getThetaEncoderValue(int i)",__FILE__,__LINE__);
}

///Return the ith value of Theta Real array	
///\exception This method returns a IndexOutOfBoundException if the value with the ith index is not found.
double SambaFileReader::getThetaRealValue(int i) const // throw (IndexOutOfBoundException)	
{

	if ((0<=i) && (i<getNbData())) 
	{
		return getYValue(i);
	}
	else throw IndexOutOfBoundException("i",i,0,getNbData(),"SambaFileReader::getThetaRealValue(int i)",__FILE__,__LINE__);
}

}

