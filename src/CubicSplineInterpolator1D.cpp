// CubicSplineInterpolator1D.cpp: implementation of the CubicSplineInterpolator1D class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "CubicSplineInterpolator1D.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
CubicSplineInterpolator1D::CubicSplineInterpolator1D()
{

}

///Destructor
CubicSplineInterpolator1D::~CubicSplineInterpolator1D()
{
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
}


///This constructor requires 3 parameters :
///		- the name of the interpolator
///		- the description of the interpolator
///		- the interpolation data
CubicSplineInterpolator1D::CubicSplineInterpolator1D(	std::string sName, 
														std::string sDescription, 
														InterpolationData1D* mInterpolationData) : 
Interpolator1D(sName,sDescription,"Cubic Spline",mInterpolationData)

{
	acc		= gsl_interp_accel_alloc ();
	spline	= gsl_spline_alloc (gsl_interp_cspline, getNbData());
	gsl_spline_init (spline, mInterpolationData->getXValues(), mInterpolationData->getYValues(), getNbData());
}

///Return the interpolated value for the wanted dValue
double CubicSplineInterpolator1D::getInterpolatedValue(double dValue)
{
      return gsl_spline_eval (spline, dValue, acc);
}

///Method to update the interpolator according the new InterpolationData values
void CubicSplineInterpolator1D::updateInterpolator()
{
	gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
	acc		= gsl_interp_accel_alloc ();
	spline	= gsl_spline_alloc (gsl_interp_cspline, getNbData());
	gsl_spline_init (spline, getInterpolatedData()->getXValues(), getInterpolatedData()->getYValues(), getNbData());
}
}


