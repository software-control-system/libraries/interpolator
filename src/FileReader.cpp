// FileReader.cpp: implementation of the FileReader class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "FileReader.h"

#include <fstream>
#include <string>
#include <iterator>
#include <stdio.h>
#include <string.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
FileReader::FileReader()
{

}

///Destructor
FileReader::~FileReader()
{

}

///This constructor requires 1 parameter :
///		- the name of the file to read
///If the file can be opened, the file is put in an buffer\n
///Thus once the file read, the file is not still in use (the file is closed)\n
///All the Data are put in the buffer and all the operations are done in the buffer not directly in the file.
///\exception This method throw a FileNotFoundException exception if the file sFileName can't be open or read.
FileReader::FileReader(const std::string& sFileName) : _sFileName(sFileName)// throw (FileNotFoundException)
{
	if(isReadable()) 
	{
		copyFileToBuffer();
		setBufferSize(extractBufferSize());
		setFileSize(extractFileSize());
		setNbFileLines(extractNbFileLines());
		extractHeader();
		if(Interpolator::_verb == true) cout << "Open file : " << sFileName << " --> OK" << endl;
	}
	else throw FileNotFoundException(sFileName,"FileReader::FileReader(const std::string& sFileName)",__FILE__,__LINE__);
}

///This constructor requires 3 parameters :
///		- the name of the file to read
///		- the number of variables (number of columns) of the file to read
///		- the number of Data (number of lines) of the file to read (without the first line which contains the differents variables names)
///If the file can be opened, the file is put in an buffer\n
///Thus once the file read, the file is not still in use (the file is closed)\n
///All the Data are put in the buffer and all the operations are done in the buffer not directly in the file.
///\exception This method throw a FileNotFoundException exception if the file sFileName can't be open or read.
FileReader::FileReader(const std::string& sFileName,long lNbVariables,long lNbData)  : _sFileName(sFileName),_lNbVariables(lNbVariables),_lNbData(lNbData) // throw (FileNotFoundException)
{
	if(isReadable()) 
	{
		copyFileToBuffer();
		setBufferSize(extractBufferSize());
	    setFileSize(extractFileSize());
		setNbFileLines(extractNbFileLines());
		if(Interpolator::_verb == true) cout << "Open file : " << sFileName << " --> OK" << endl;
	}
	else throw FileNotFoundException(sFileName,"FileReader::FileReader(const std::string& sFileName,long lNbVariables,long lNbData)",__FILE__,__LINE__);
}


///This method test if the file can be open and read.
bool FileReader::isReadable() 
{ 

	if (_sFileName.empty()) return false;
	else
	{
		ifstream mFile( _sFileName.c_str() ); 
        if (mFile) return true;
        else return false;

        //- does not compil on vc12 (c++11)
		//return mFile != 0;
	}
     
} 

///Return the size of the file.
long FileReader::extractFileSize() // throw (FileNotFoundException)
{
	ifstream mFile(_sFileName.c_str());
    long pos,size;
	if (mFile) 
	{
		pos = mFile.tellg();
		mFile.seekg( 0 ,	ios_base::end );
		size = mFile.tellg() ;
		mFile.seekg( pos,	ios_base::beg );
	}
	else throw FileNotFoundException(_sFileName,"FileReader::extractNbFileLines()",__FILE__,__LINE__);
	return size;
}

/*///Extract the number of columns (for file providing a header with the number of columns in it)
long FileReader::extractNbVariables()
{	
	ifstream mFile(_sFileName.c_str());
 	if (mFile) 
	{
		_lNbVariables = extractInt();
	}
	return _lNbVariables;
}
*/
/*///Extract the number of lines (for file providing a header with the number of lines in it)
long FileReader::extractNbData()
{
	_lNbData = 20;
	return _lNbData;
}
*/

void FileReader::extractHeader()
{
	ifstream mFile(_sFileName.c_str());
 	if (mFile) 
	{
		_lNbVariables	= extractInt();
		_lNbData		= extractInt();
	}
}


///Return the number of lines of the file.
///\exception This method throw a FileNotFoundException exception if the file can't be open or read.
long FileReader::extractNbFileLines() // throw (FileNotFoundException)
{
    ifstream mFile(_sFileName.c_str()); 
	int iLines = 0;
    if ( mFile )
    {
        iLines = count(	istreambuf_iterator<char>( mFile ),
						istreambuf_iterator<char>(),
						'\n' );
    }
	else throw FileNotFoundException(_sFileName,"FileReader::extractNbFileLines()",__FILE__,__LINE__);
	return iLines+1;
}


///Return a complete line of the buffer.
std::string FileReader::extractLine() 
{
	std::string sLine;
	getline(_mFileBuffer, sLine );
	return sLine;
}

///Extract a double from the buffer.
double FileReader::extractDouble() 
{

  std::string sTemp;
  sTemp = extractString();
  return atof(sTemp.c_str());
}

///Extract a int from the buffer.
int FileReader::extractInt() 
{
	std::string sTemp;
	sTemp = extractString();
	return atoi(sTemp.c_str());
}

///Extract a std::string from the buffer.
std::string FileReader::extractString() 
{
	std::string sTemp;
	_mFileBuffer >> sTemp;
	return sTemp;
}

///Compute the size of the buffer.
long FileReader::extractBufferSize() 
{
	return (long)_mFileBuffer.str().size();
}

///This method is used to copy the file in the buffer\n
///After the copy is done the file is closed.
///\exception This method throw a FileNotFoundException exception if the file can't be open or read.
void FileReader::copyFileToBuffer() // throw (FileNotFoundException)
{
	ifstream mFile(_sFileName.c_str());

    if ( mFile ) 
    {
		_mFileBuffer << mFile.rdbuf();
        mFile.close();
		setBufferSize(extractBufferSize());
    }
	else throw FileNotFoundException(_sFileName,"FileReader::copyFileToBuffer()",__FILE__,__LINE__);
}

///Return the file size
long FileReader::getFileSize() const
{
	return _lFileSize;
}

///Return the number of lines of the file.
long FileReader::getNbFileLines() const
{
    return _lNbFileLines;
}

///Return the number of Data (number of lines of Data) of the file.
long FileReader::getNbData() const
{
	return _lNbData;
}

///Return the number of variables (the number of columns) of the file.
long FileReader::getNbVariables() const
{
    return _lNbVariables;
}

///Return the size of the buffer containing the file.
long FileReader::getBufferSize() const
{
	return _lBufferSize;
}

///Set the size of the buffer containing the file.
void FileReader::setBufferSize(long lBufferSize)
{
	_lBufferSize = lBufferSize;
}

///Set the number of variables (the number of columns).
void FileReader::setNbVariables(long lNbVariables)
{
	_lNbVariables = lNbVariables;
}

///Set the file size	
void FileReader::setFileSize(long lFileSize)
{
	_lFileSize = lFileSize;
}
	
///Set the number of lines in the file.
void FileReader::setNbFileLines(long lNbFileLines)
{
	_lNbFileLines = lNbFileLines;
}

///Set the number of Data (number of lines of Data).	
void FileReader::setNbData(long lNbData) 
{
	_lNbData = lNbData;
}


}

