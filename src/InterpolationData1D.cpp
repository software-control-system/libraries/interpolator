// InterpolationData1D.cpp: implementation of the InterpolationData class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "InterpolationData1D.h"
#include "NotAllowedOperationException.h"
#include <iostream>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

namespace Interpolator
{
///Default constructor
InterpolationData1D::InterpolationData1D() : _sXName(""),_sYName(""),_lNbData(0),_mXValues(0),_mYValues(0)
{

}

///Destructor
InterpolationData1D::~InterpolationData1D() 
{
	if (_mXValues)
	{
		delete [] _mXValues;
		_mXValues = 0;	
	}
	
	if (_mYValues)
	{
		delete [] _mYValues;
		_mYValues = 0;	
	}
}

///This constructor requires 5 parameters :
///		- the name of the X values
///		- the name of the Y values
///		- the number of values
///		- the X values array
///		- the Y values array
InterpolationData1D::InterpolationData1D(	std::string sXName, std::string sYName, 
											long lNbData,
											double* mXValues, double* mYValues) : 
_sXName(sXName),_sYName(sYName),_lNbData(lNbData),_mXValues(0),_mYValues(0)
{			
		_mXValues = new double[_lNbData];
		_mYValues = new double[_lNbData];

		setValues(lNbData,mXValues,mYValues);
}

///Return the X name (first column --> entry point for the interpolation)
std::string InterpolationData1D::getXName() const
{
	return _sXName;
}

///Return the Y name (second column --> value interpolated)
std::string InterpolationData1D::getYName() const
{
	return _sYName;
}

///Return the ith value of X array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the X array is null	
double InterpolationData1D::getXValue(int i) const // throw (IndexOutOfBoundException,NullPointerException)
{
	
	if ((i < 0) || (i >= getNbData())) throw IndexOutOfBoundException("index",i,0,getNbData(),"InterpolationData1D::getXValue(int i)",__FILE__,__LINE__); 
	if (_mXValues == 0) throw NullPointerException("_mXValues","InterpolationData1D::getXValue(int i)",__FILE__,__LINE__);
	return _mXValues[i];
}

///Return the ith value of Y array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the Y array is null	
double InterpolationData1D::getYValue(int i) const // throw (IndexOutOfBoundException,NullPointerException)
{
	if ((i < 0) || (i >= getNbData())) throw IndexOutOfBoundException("index",i,0,getNbData(),"InterpolationData1D::getYValue(int i)",__FILE__,__LINE__); 
	if (_mYValues == 0) throw NullPointerException("_mYValues","InterpolationData1D::getYValue(int i)",__FILE__,__LINE__);
	return _mYValues[i];
}

///Return the number of data
long InterpolationData1D::getNbData() const
{
	return _lNbData;
}


///Set the ith value of X array
///THE NEW VALUE MUST LET THE X ARRAY INCREASINGLY ORDERED !!!!
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the X array is null	
void InterpolationData1D::setXValue(int i,double dNewValue) // throw (IndexOutOfBoundException,NullPointerException)
{
	if ((i < 0) || (i >= getNbData())) throw IndexOutOfBoundException("index",i,0,getNbData(),"InterpolationData1D::setXValue(int i,double dNewValue)",__FILE__,__LINE__); 
	if (_mXValues == 0) throw NullPointerException("_mXValues","InterpolationData1D::setXValue(int i,double dNewValue)",__FILE__,__LINE__);
	_mXValues[i] = dNewValue;
}

///Set the ith value of Y array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the Y array is null	
void InterpolationData1D::setYValue(int i,double dNewValue) // throw (IndexOutOfBoundException,NullPointerException)
{
	if ((i < 0) || (i >= getNbData())) throw IndexOutOfBoundException("index",i,0,getNbData(),"InterpolationData1D::setYValue(int i,double dNewValue)",__FILE__,__LINE__); 
	if (_mYValues == 0) throw NullPointerException("_mYValues","InterpolationData1D::setYValue(int i,double dNewValue)",__FILE__,__LINE__);
	_mYValues[i] = dNewValue;
}


///Return the X values (first column)
///\exception This method throw a NullPointerException exception if the X array is null	
double* InterpolationData1D::getXValues() const // throw (NullPointerException)
{
	if (_mXValues == 0) throw NullPointerException("_mXValues","InterpolationData1D::getXValues()",__FILE__,__LINE__);
	return _mXValues;
}

///Return the Y values (second column)
///\exception This method throw a NullPointerException exception if the Y array is null	
double* InterpolationData1D::getYValues() const // throw (NullPointerException)
{
	if (_mYValues == 0) throw NullPointerException("_mYValues","InterpolationData1D::getYValues()",__FILE__,__LINE__);
	return _mYValues;
}


///This method allows to change the values of the X column
///THE NEW VALUES MUST LET THE X ARRAY INCREASINGLY ORDERED !!!!
///It is required that the array size is not changed
void InterpolationData1D::setXValues(double* dNewValues)
{
	for (int i=0;i<_lNbData;i++)
	{
		setXValue(i,*(dNewValues+i));
	}
}

///This method allows to change the values of the Y column
///It is required that the array size is not changed
void InterpolationData1D::setYValues(double* dNewValues)
{   
    for (int i=0;i<_lNbData;i++)
	{
		setYValue(i,*(dNewValues+i));
	}   
}


///This method allows to change the values of the X column in inverse order (useful when data are not increasingly ordered)
///THE NEW VALUES MUST LET THE X ARRAY INCREASINGLY ORDERED !!!!
///It is required that the array size is not changed
void InterpolationData1D::setXValuesInInverseOrder(double* dNewValues)
{
	for (int i=0;i<_lNbData;i++)
	{
		setXValue(i,*(dNewValues + _lNbData - i - 1));
	}
}

///This method allows to change the values of the Y column in inverse order (useful when data are not increasingly ordered)
///It is required that the array size is not changed
void InterpolationData1D::setYValuesInInverseOrder(double* dNewValues)
{   
    for (int i=0;i<_lNbData;i++)
	{
		setYValue(i,*(dNewValues + _lNbData - i - 1));
	}   
}




///This method allows to change the values of the X and Y columns\n
///If the array size is changed, memory is desallocated and new arrays are reallocated to match the new size
void InterpolationData1D::setValues(long lNbData,double* dNewXValues,double* dNewYValues)
{

switch(checkDataValidity(lNbData,dNewXValues))
	{
	case -1 :	//the data are ok but decreasingly ordered (must be reorder in order to work) 
		checkSize(lNbData);
		if(Interpolator::_verb == true) cout << "DATA NOT ORDERED --> ORDERING FOR YOU !!!" << endl;
		setXValuesInInverseOrder(dNewXValues);
		setYValuesInInverseOrder(dNewYValues);
		break;
	case 1 :	//the data are ok and in increasingly ordered

		setXValues(dNewXValues);
		setYValues(dNewYValues);
		break;
	case 0 :	//the data are not ok they are not ordered
		{
		std::string sTableName = "(";
					sTableName+=getXName();
					sTableName+=" --> ";
					sTableName+=getYName();
					sTableName+=")";

		throw NotAllowedOperationException(	sTableName,
											"The values of the table are not ordered !!!  --> Check the table values",
											"void InterpolationData1D::setValues(long lNbData,double* dNewXValues,double* dNewYValues)",
											__FILE__,
											__LINE__);									
		break;
		}
	case -2 :	//two data are identical (must remove one)
		{
			std::string sTableName = "(";
					sTableName+=getXName();
					sTableName+=" --> ";
					sTableName+=getYName();
					sTableName+=")";
		throw NotAllowedOperationException(	sTableName,
											"Two values of the entry column are the same !!! --> Check the table values",
											"void InterpolationData1D::setValues(long lNbData,double* dNewXValues,double* dNewYValues)",
											__FILE__,
											__LINE__);

		break;
		}
	default :
		{
		std::string sTableName = "(";
					sTableName+=getXName();
					sTableName+=" --> ";
					sTableName+=getYName();
					sTableName+=")";
		throw NotAllowedOperationException(	sTableName,
											"Unknow Exception during the set values of the table --> Check the table values",
											"void InterpolationData1D::setValues(long lNbData,double* dNewXValues,double* dNewYValues)",
											__FILE__,
											__LINE__);
		}
	}
}


void InterpolationData1D::checkSize(long lNbData)
{
	if (lNbData != getNbData()) //It is necessary to reallocate the arrays
    { 
		delete [] _mXValues;
		delete [] _mYValues;
		
		_lNbData = lNbData;

		_mXValues = new double[_lNbData];
		_mYValues = new double[_lNbData];
    }
}







///This method allow to check if the data are valids
///One limitation of the interpolator of GSL is that the data must be ordered increasingly.
///It is necessary to check that the data are ordered, moreover in increasing order and that there are no two same values
///Return an int which indicate how are the data
/// 1	: the data are ok and in increasingly ordered
/// -1	: the data are ok but decreasingly ordered (must be reorder in order to work)
/// 0	: the data are not ok they are not ordered
/// -2  : two data are identical (must remove one) 
int InterpolationData1D::checkDataValidity(long lNbData,double* dNewXValues)
{
	double dFirstValue	= *dNewXValues;
	double dSecondValue = *(dNewXValues+1);
	int i;
	bool	bIsIncreasing = true;

	//Check the possible ordering

	if (dFirstValue == dSecondValue)		//The two first values are identical : problem !!!
	{
		return -2;
	}
	else if (dFirstValue < dSecondValue)	//All the values must be in increasing order
	{
		bIsIncreasing = true;
	}
	else									//All the values must be in decreasing order
	{
		bIsIncreasing = false;
	}
	
	double dLastValue = dSecondValue;
	double dNewValue;

	//Check the ordering of the values
	for (i=2;i<lNbData;i++)
	{
		dNewValue = *(dNewXValues+i);
		
		if (bIsIncreasing)
		{
			if (dNewValue < dLastValue) return 0;
			else if (dNewValue == dLastValue) return -2;
		}
		else
		{
			if (dNewValue > dLastValue) return 0;
			else if (dNewValue == dLastValue) return -2;
		}

		dLastValue = dNewValue;
	}	

	//In this case it's ok the data are ordered and with the boolean i know if i need to sort or no the values
	if (i == lNbData)
	{
		if (bIsIncreasing) return 1;
		else return -1;
	}
	
	return 2; //This case must never happen
}



///Display the data\n Used for debug
void InterpolationData1D::DisplayData()
{
	if(Interpolator::_verb == true) cout << "\t" << getXName() << "\t\t " << getYName() << endl;
	for (int i=0;i<getNbData();i++)
	{
		if(Interpolator::_verb == true) cout << i << "\t" << getXValue(i) << "\t\t " << getYValue(i) << endl;		
	}
}





}





