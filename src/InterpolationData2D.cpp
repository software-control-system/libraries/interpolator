// InterpolationData2D.cpp: implementation of the InterpolationData2D class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32 
#pragma warning(disable:4786)
#endif

#include "InterpolationData2D.h"
#include <iostream>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
InterpolationData2D::InterpolationData2D()
{

}

///Destructor
InterpolationData2D::~InterpolationData2D()
{
	delete [] _mXValues;
	delete [] _mYValues;
	delete [] _mZValues;
	_mXValues = 0;
	_mYValues = 0;
	_mZValues = 0;
}

///This constructor requires 8 parameters :
///		- the name of the X values
///		- the name of the Y values
///		- the name of the Z values
///		- the number of X values
///		- the number of Y values
///		- the X values array
///		- the Y values array
///		- the Z values array
InterpolationData2D::InterpolationData2D(	std::string sXName, std::string sYName, std::string sZName,
											long lNbXData,long lNbYData,
											double* mXValues, double* mYValues, double* mZValues) : 
_sXName(sXName),_sYName(sYName),_sZName(sZName),
_lNbXData(lNbXData),_lNbYData(lNbYData),_lNbZData(lNbXData*lNbYData)
{

	_mXValues = new double[_lNbXData];
	_mYValues = new double[_lNbYData];
	_mZValues = new double[_lNbZData];

	setValues(_lNbXData,_lNbYData,mXValues,mYValues,mZValues);
}

///Return the X name (first column)
std::string InterpolationData2D::getXName() const
{
	return _sXName;
}

///Return the Y name (first line)
std::string InterpolationData2D::getYName() const
{
	return _sYName;
}

///Return the Z name (matrix)
std::string InterpolationData2D::getZName() const
{
	return _sZName;
}


///Return the ith value of X array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the X array is null	
double InterpolationData2D::getXValue(int i) const // throw (IndexOutOfBoundException,NullPointerException)
{
	if ((i < 0) || (i >= getNbXData())) throw IndexOutOfBoundException("index",i,0,getNbXData(),"InterpolationData2D::getXValue(int i)",__FILE__,__LINE__); 
	if (_mXValues == 0) throw NullPointerException("_mXValues","InterpolationData2D::getXValue(int i)",__FILE__,__LINE__);
	return _mXValues[i];
}

///Return the ith value of Y array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the Y array is null	
double InterpolationData2D::getYValue(int i) const // throw (IndexOutOfBoundException,NullPointerException)
{
	if ((i < 0) || (i >= getNbYData())) throw IndexOutOfBoundException("index",i,0,getNbYData(),"InterpolationData2D::getYValue(int i)",__FILE__,__LINE__); 
	if (_mYValues == 0) throw NullPointerException("_mYValues","InterpolationData2D::getYValue(int i)",__FILE__,__LINE__);
	return _mYValues[i];
}

///Return the (ith,jth) value of Z array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the Z array is null	
double InterpolationData2D::getZValue(int i,int j) const // throw (IndexOutOfBoundException,NullPointerException)
{
	if ((i < 0) || (i >= getNbXData())) throw IndexOutOfBoundException("index",i,0,getNbXData(),"InterpolationData2D::getZValue(int i,int j)",__FILE__,__LINE__); 
	if ((j < 0) || (j >= getNbYData())) throw IndexOutOfBoundException("index",j,0,getNbYData(),"InterpolationData2D::getZValue(int i,int j)",__FILE__,__LINE__); 
	if (_mZValues == 0) throw NullPointerException("_mZValues","InterpolationData2D::getZValue(int i,int j)",__FILE__,__LINE__);
	return _mZValues[i*_lNbYData + j];
}


///Return the number of X data
long InterpolationData2D::getNbXData() const
{
	return _lNbXData;
}

///Return the number of Y data
long InterpolationData2D::getNbYData() const
{
	return _lNbYData;
}

///Return the number of Z data
long InterpolationData2D::getNbZData() const
{
	return _lNbZData;
}





///Set the ith value of X array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the X array is null	
void InterpolationData2D::setXValue(int i,double dNewValue) // throw (IndexOutOfBoundException,NullPointerException)
{
	if ((i < 0) || (i >= getNbXData())) throw IndexOutOfBoundException("index",i,0,getNbXData(),"InterpolationData2D::setXValue(int i,double dNewValue)",__FILE__,__LINE__); 
	if (_mXValues == 0) throw NullPointerException("_mXValues","InterpolationData2D::setXValue(int i,double dNewValue)",__FILE__,__LINE__);
	_mXValues[i] = dNewValue;
}

///Set the ith value of Y array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the Y array is null	
void InterpolationData2D::setYValue(int i,double dNewValue) // throw (IndexOutOfBoundException,NullPointerException)
{
	if ((i < 0) || (i >= getNbYData())) throw IndexOutOfBoundException("index",i,0,getNbYData(),"InterpolationData2D::setYValue(int i,double dNewValue)",__FILE__,__LINE__); 
	if (_mYValues == 0) throw NullPointerException("_mYValues","InterpolationData2D::setYValue(int i,double dNewValue)",__FILE__,__LINE__);
	_mYValues[i] = dNewValue;
}


///Set the (ith,jth) value of Z array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the Z array is null	
void InterpolationData2D::setZValue(int i,int j,double dNewValue) // throw (IndexOutOfBoundException,NullPointerException)
{
	if ((i < 0) || (i >= getNbXData())) throw IndexOutOfBoundException("index",i,0,getNbXData(),"InterpolationData2D::setZValue(int i,int j,double dNewValue)",__FILE__,__LINE__); 
	if ((j < 0) || (j >= getNbYData())) throw IndexOutOfBoundException("index",j,0,getNbYData(),"InterpolationData2D::setZValue(int i,int j,double dNewValue)",__FILE__,__LINE__); 
	if (_mZValues == 0) throw NullPointerException("_mZValues","InterpolationData2D::setZValue(int i,int j,double dNewValue)",__FILE__,__LINE__);
	_mZValues[i*_lNbYData + j] = dNewValue;
}

///Return the X array
///\exception This method throw a NullPointerException exception if the X array is null	
double* InterpolationData2D::getXValues() const // throw (NullPointerException)
{
	if (_mXValues == 0) throw NullPointerException("_mXValues","InterpolationData2D::getXValues()",__FILE__,__LINE__);
	return _mXValues;
}

///Return the Y array
///\exception This method throw a NullPointerException exception if the Y array is null	
double* InterpolationData2D::getYValues() const // throw (NullPointerException)
{
	if (_mYValues == 0) throw NullPointerException("_mYValues","InterpolationData2D::getYValues()",__FILE__,__LINE__);
	return _mYValues;
}

///Return the Z array
///\exception This method throw a NullPointerException exception if the Z array is null	
double* InterpolationData2D::getZValues() const // throw (NullPointerException)
{
	if (_mZValues == 0) throw NullPointerException("_mZValues","InterpolationData2D::getZValues()",__FILE__,__LINE__);
	return _mZValues;
}


///This method allows to change the values of the X column
///It is required that the array size is not changed
void InterpolationData2D::setXValues(double* dNewValues)
{
    for (int i=0;i<_lNbXData;i++)
	{
	  setXValue(i,*(dNewValues+i));
	}
}

///This method allows to change the values of the Y line
///It is required that the array size is not changed
void InterpolationData2D::setYValues(double* dNewValues)
{   
    for (int i=0;i<_lNbYData;i++)
	{
	  setYValue(i,*(dNewValues+i));
	}
}


///This method allows to change the values of the Z matrix
///It is required that the array size is not changed
void InterpolationData2D::setZValues(double* dNewValues)
{   
 for (int i=0;i<_lNbXData;i++)
 {
	for (int j=0;j<_lNbYData;j++)
	{
		setZValue(i,j,*(dNewValues+(i*_lNbYData + j)));
	}
 }
}


///This method allows to change the values of the X,Y and Z arrays\n
///If the matrix size is changed, memory is desallocated and new arrays are reallocated to match the new sizes
void InterpolationData2D::setValues(long lNbXData,long lNbYData,double* dNewXValues,double* dNewYValues,double* dNewZValues)
{

  if (lNbXData != getNbXData() || lNbYData != getNbYData()) //It is necessary to reallocate the X and Z array
  { 
	delete [] _mXValues;
	delete [] _mYValues;
	delete [] _mZValues;
	
	_lNbXData = lNbXData;
	_lNbYData = lNbYData;
	_lNbZData = lNbXData * lNbYData;

	_mXValues = new double[_lNbXData];
	_mYValues = new double[_lNbYData];
	_mZValues = new double[_lNbZData];
   }
	
  setXValues(dNewXValues);
  setYValues(dNewYValues);
  setZValues(dNewZValues);
}


///Display the interpolation 2D data\n
///Used mainly for DEBUG
void InterpolationData2D::printInfos()
{
	int i,j;

	if(Interpolator::_verb == true) cout << "\n##### INTERPOLATION DATA 2D #####" << endl;
	if(Interpolator::_verb == true) cout << "X Name=" << getXName() << " | Y Name=" << getYName() << " | Z Name=" << getZName() << endl;
	if(Interpolator::_verb == true) cout << "\t";

	for (i=0;i < getNbYData();i++) 
	{
		if(Interpolator::_verb == true) cout << getYValue(i) << "\t";
	}
	if(Interpolator::_verb == true) cout << endl;

	for (i=0;i< getNbXData();i++)
	{
		for (j=-1;j < getNbYData();j++)
		{
			if (j==-1)
			{
				if(Interpolator::_verb == true) cout << getXValue(i) << "\t";
			}
			else if(Interpolator::_verb == true) cout << getZValue(i,j) << "\t";
		}
		if(Interpolator::_verb == true) cout << endl;
	}

}

}

