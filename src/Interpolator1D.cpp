// Interpolator1D.cpp: implementation of the Interpolator1D class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "Interpolator1D.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
Interpolator1D::Interpolator1D()
{
}

///Destructor
Interpolator1D::~Interpolator1D()
{

}

///This constructor requires 4 parameters :
///		- the name of the interpolator
///		- the description of the interpolator
///		- the interpolated type
///		- the interpolation data object
Interpolator1D::Interpolator1D(	std::string sName, 
								std::string sDescription, 
								std::string sInterpolationType,
								InterpolationData1D* mInterpolationData) : 
Interpolator(sName,sDescription,sInterpolationType),_mInterpolationData(mInterpolationData)
{			
}

///Return the number of data provided for the interpolation	
long	Interpolator1D::getNbData()
{
	return _mInterpolationData->getNbData();
}

///Return the 1D interpolation data object
InterpolationData1D* Interpolator1D::getInterpolatedData()
{
	return _mInterpolationData;
}

}


