// Table.cpp: implementation of the Table class.
//
//////////////////////////////////////////////////////////////////////

#include "Table.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
Table::Table() : ComputingObject()
{

}


///Destructor
Table::~Table()
{

}

///This constructor requires 4 parameters :
///		- the name of the table
///		- a description of the table
///		- the wanted interpolation type 
///		- the file path if the table is constructed thanks a file (optional)
Table::Table(std::string sName,std::string sDescription,std::string sInterpolationType,std::string sFilePath) : ComputingObject(),
_sName(sName),_sDescription(sDescription),_sInterpolationType(sInterpolationType),_sFilePath(sFilePath)
{

}

///Return the name of the table
std::string Table::getName() const
{
	return _sName;
}
	
///Return the description of the table
std::string Table::getDescription() const
{
	return _sDescription;
}

///Return the interpolation type of the table
std::string Table::getInterpolationType() const
{
	return _sInterpolationType;
}

///Return the file path used to initialized the Table (if a file is used) 
std::string Table::getFilePath() const
{
	return _sFilePath;
}


}

