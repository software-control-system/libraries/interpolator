// Interpolator2D.cpp: implementation of the Interpolator2D class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "Interpolator2D.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
Interpolator2D::Interpolator2D()
{

}

///Destructor
Interpolator2D::~Interpolator2D()
{

}

///This constructor requires 4 parameters :
///		- the name of the interpolator
///		- the description of the interpolator
///		- the interpolated type
///		- the interpolation data object
Interpolator2D::Interpolator2D(	std::string sName, 
								std::string sDescription,
								std::string sInterpolationType,	
								InterpolationData2D* mInterpolationData) : 
Interpolator(sName,sDescription,sInterpolationType),_mInterpolationData(mInterpolationData)

{
				
}

///Method to find the nearest indexes of the two values dXValue, dYValue
void Interpolator2D::findIndexes(double dXValue,double dYValue)
{
	//Faire algo quicksort pour plus de rapidit�
	//int iXIndex = 
	findXIndex(dXValue);
	//int iYIndex = 
	findYIndex(dYValue);
}

///Method to find the nearest index of the dXValue
int Interpolator2D::findXIndex(double dXValue)
{
	//Faire algo quicksort pour plus de rapidit�
	int iXIndex = -1;

	for (int i=0;i<_mInterpolationData->getNbXData();i++)
	{
		if (_mInterpolationData->getXValue(i) > dXValue) return iXIndex;
		else iXIndex = i;
	}
	return iXIndex+1;
}

///Method to find the nearest index of the dYValue
int Interpolator2D::findYIndex(double dYValue)
{
	//Faire algo quicksort pour plus de rapidit�
	int iYIndex = -1;

	for (int i=0;i<_mInterpolationData->getNbYData();i++)
	{
		if (_mInterpolationData->getYValue(i) > dYValue) return iYIndex;
		else iYIndex = i;
	}
	return iYIndex+1;
}

///Return the 2D interpolation data object
InterpolationData2D* Interpolator2D::getInterpolatedData()
{
	return _mInterpolationData;
}


}

