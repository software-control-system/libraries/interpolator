// NearestNeighbourInterpolator2D.cpp: implementation of the NearestNeighbourInterpolator2D class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "NearestNeighbourInterpolator2D.h"
#include "Tools.h"
#include <iostream>
#include <vector>
#include <algorithm>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
NearestNeighbourInterpolator2D::NearestNeighbourInterpolator2D()
{

}

///Destructor
NearestNeighbourInterpolator2D::~NearestNeighbourInterpolator2D()
{

}

///This constructor requires 3 parameters :
///		- the name of the interpolator
///		- the description of the interpolator
///		- the interpolation type (must be "Nearest")
///		- the interpolation data
NearestNeighbourInterpolator2D::NearestNeighbourInterpolator2D(	std::string sName,
																std::string sDescription,
																std::string sInterpolationType,
																InterpolationData2D* mInterpolationData) : 
Interpolator2D(sName,sDescription,sInterpolationType,mInterpolationData)
{
}


///Return the interpolated value for the wanted couple (dXValue,dYvalue)
double NearestNeighbourInterpolator2D::getInterpolatedValue(double dXValue,double dYValue)
{
	double z = compute(dXValue,dYValue);
	return z;
}

///Compute the interpolated value
double NearestNeighbourInterpolator2D::compute(double dXValue,double dYValue)
{
	//Faire algo quicksort pour plus de rapidit� et faire les verifs sur les index calcul�s et sur le z si possible 
	int iXIndex = findXIndex(dXValue);
	int iYIndex = findYIndex(dYValue);
	
	double z,z1,z2,z3,z4,x1,x3,y1,y2;

	z = 0;
	//The 4 cases where the indexes for X and Y are outside the arrays (four corners cases)
	if ((iXIndex >= _mInterpolationData->getNbXData()) && (iYIndex >= _mInterpolationData->getNbYData()))
	{
		if(Interpolator::_verb == true) cout << "case 1" << endl;
		z = _mInterpolationData->getZValue(iXIndex-1,iYIndex-1);
	}
	
	else if ((iXIndex == -1) && (iYIndex == -1))
	{	
		if(Interpolator::_verb == true) cout << "case 2" << endl;
		z = _mInterpolationData->getZValue(0,0);
	}

	else if ((iXIndex == -1) && (iYIndex >= _mInterpolationData->getNbYData()))
	{
		if(Interpolator::_verb == true) cout << "case 3" << endl;
		z = _mInterpolationData->getZValue(0,iYIndex-1);
	}

	else if ((iXIndex >= _mInterpolationData->getNbXData()) && (iYIndex == -1))
	{
		if(Interpolator::_verb == true) cout << "case 4" << endl;
		z = _mInterpolationData->getZValue(iXIndex-1,0);
	}


	//The case when the X index is outside (bigger than the last index) of the array
	else if (iXIndex >= _mInterpolationData->getNbXData()) 
	{
		if(Interpolator::_verb == true) cout << "case 5" << endl;
		x1	=  _mInterpolationData->getXValue(iXIndex-1);
		 
		y1 =  _mInterpolationData->getYValue(iYIndex);
		y2 =  _mInterpolationData->getYValue(iYIndex+1);

		z1	= _mInterpolationData->getZValue(iXIndex-1,iYIndex);
		z2	= _mInterpolationData->getZValue(iXIndex-1,iYIndex+1);		
	
		double dDistance1 = x1*x1 + y1*y1;
		double dDistance2 = x1*x1 + y2*y2;
		
		if (isLessThan(dDistance1,dDistance2)) z =  z1;
		else z =  z2;
	}
	
	//The case when the X index is outside (lower than the first index) of the array
	else if (iXIndex == -1) 
	{
		if(Interpolator::_verb == true) cout << "case 6" << endl;
		 x1	=  _mInterpolationData->getXValue(iXIndex+1);
		 
		 y1 =  _mInterpolationData->getYValue(iYIndex);
		 y2 =  _mInterpolationData->getYValue(iYIndex+1);

		 z1	= _mInterpolationData->getZValue(iXIndex+1,iYIndex);
		 z2	= _mInterpolationData->getZValue(iXIndex+1,iYIndex+1);		
		
		double dDistance1 = x1*x1 + y1*y1;
		double dDistance2 = x1*x1 + y2*y2;
		
		if (isLessThan(dDistance1,dDistance2)) z =  z1;
		else z =  z2;
	}

	//The case when the Y index is outside (bigger than the last index) of the array
	else if (iYIndex >= _mInterpolationData->getNbYData())
	{
		if(Interpolator::_verb == true) cout << "case 7" << endl;
		 y1 =  _mInterpolationData->getYValue(iYIndex-1);

		 z1	= _mInterpolationData->getZValue(iXIndex,iYIndex-1);
		 z3	= _mInterpolationData->getZValue(iXIndex+1,iYIndex-1);		
	
		 x1	=  _mInterpolationData->getXValue(iXIndex);
		 x3	=  _mInterpolationData->getXValue(iXIndex+1);
		
		double dDistance1 = x1*x1 + y1*y1;
		double dDistance2 = x3*x3 + y1*y1;
		
		if (isLessThan(dDistance1,dDistance2)) z =  z1;
		else z =  z3;
	}
	
		
	//The case when the Y index is outside (lower than the first index) of the array
	else if (iYIndex == -1 )
	{
		if(Interpolator::_verb == true) cout << "case 8" << endl;
		 y1 =  _mInterpolationData->getYValue(iYIndex+1);

		 z1	= _mInterpolationData->getZValue(iXIndex,iYIndex+1);
		 z3	= _mInterpolationData->getZValue(iXIndex+1,iYIndex+1);		
	
		 x1	=  _mInterpolationData->getXValue(iXIndex);
		 x3	=  _mInterpolationData->getXValue(iXIndex+1);
		
		double dDistance1 = x1*x1 + y1*y1;
		double dDistance2 = x3*x3 + y1*y1;
		
		if (isLessThan(dDistance1,dDistance2)) z = z1;
		else z =  z3;
	}


	//Normal case
	else 
	{
		if(Interpolator::_verb == true) cout << "case 9" << endl;
		 z1	= _mInterpolationData->getZValue(iXIndex,iYIndex);
		 z2	= _mInterpolationData->getZValue(iXIndex,iYIndex+1);	
		 z3	= _mInterpolationData->getZValue(iXIndex+1,iYIndex+1);	
		 z4	= _mInterpolationData->getZValue(iXIndex+1,iYIndex);	
		
		 x1	=  _mInterpolationData->getXValue(iXIndex);
		 x3	=  _mInterpolationData->getXValue(iXIndex+1);
		 y1	=  _mInterpolationData->getYValue(iYIndex);
		 y2	=  _mInterpolationData->getYValue(iYIndex+1);	
		 
		double dDistance1 = x1*x1 + y1*y1;
		double dDistance2 = x1*x1 + y2*y2;
		double dDistance3 = x3*x3 + y2*y2;
		double dDistance4 = x3*x3 + y1*y1;

		std::vector<double> distanceVector;
		distanceVector.push_back(dDistance1);
		distanceVector.push_back(dDistance2);
		distanceVector.push_back(dDistance3);
		distanceVector.push_back(dDistance4);
		sort(distanceVector.begin(), distanceVector.end());
		
		if (distanceVector[0] == dDistance1) z = z1;
		else if (distanceVector[0] == dDistance2) z = z2;
		else if (distanceVector[0] == dDistance3) z = z3;
		else if (distanceVector[0] == dDistance4) z = z4;

	}
	if(Interpolator::_verb == true) cout << _mInterpolationData->getZName() << "=f(" << _mInterpolationData->getXName() << "," << _mInterpolationData->getYName() << ")=f("<< dXValue << "," << dYValue<< ")=" <<  z << endl;
	//if(Interpolator::_verb == true) cout << "z=f("<< dXValue << "," << dYValue<< ")=" <<  z << endl;
	return z;
}
}


