#=============================================================================
#
# file :        Makefile.h
#
# description : Makefile for DeviceServer
#
# $Author: flanglois $
#
# $Revision: 1.4 $
#
# $Log: not supported by cvs2svn $
# Revision 1.3  2009/01/20 16:23:24  flanglois
# - minor modifs in makefiles
#
# Revision 1.2  2009/01/16 16:29:13  flanglois
# - added Interpolator method from H. D'Haussy/V. Delos (DataFile*)
#
# Revision 1.1  2008/11/21 13:16:12  stephle
# initial import
#
# Revision 1.2  2006/01/05 16:51:30  hardion
# * Update Makefile.linux and tango.opt for library use
#
#
#=============================================================================



#=============================================================================
# OUTPUT_TYPE can be one of the following :
#   - 'STATIC_LIB' for a static library (.a)
#   - 'DYNAMIC_LIB' for a dynamic library (.so)
#   - 'DEVICE' for a device server (will automatically include and link
#            with Tango dependencies)
#   - 'SIMPLE_EXE' for an executable with no dependency (for exemple the test tool
#                of a library with no Tango dependencies)
#
OUTPUT_TYPE = STATIC_LIB
OUTPUT_DIR = ../lib



#=============================================================================
# TANGO_REQUIRED 
#=============================================================================
# - TRUE  : your project depends on TANGO
# - FALSE : your project does not depend on TANGO
#-----------------------------------------------------------------------------
# - NOTE : if PROJECT_TYPE is set to DEVICE, TANGO will be auto. added
#-----------------------------------------------------------------------------  
TANGO_REQUIRED = TRUE



#=============================================================================
# PACKAGE_NAME is the name of the library/device/exe you want to build
#   - for a device server, PACKAGE_NAME will be prefixed by 'ds_'
#   - for a library (static or dynamic), PACKAGE_NAME will be prefixed by 'lib'
#   - for a simple executable, PACKAGE_NAME will be the name of the executable
#

ifeq ($(RELEASE_TYPE),DEBUG)
	PACKAGE_NAME= Interpolatord
else
	PACKAGE_NAME= Interpolator
endif





#=============================================================================
# INC_DIR_USER is the list of all include path needed by your sources
#   - for a device server, tango dependencies are automatically appended
#   - '-I ../include' and '-I .' are automatically appended in all cases
#

# !!!!!!!!!
# Please create an ENV VARIABLE called: SW_SUPPORT pointing to your library path (Utils,Exceptions...)
# eg:   export SW_SUPPORT=../..

INC_DIR_USER= 	-I $(SW_SUPPORT)/GSL/include/ \
				-I $(SW_SUPPORT)/Utils/include/  \


#=============================================================================
# LIB_DIR_USER is the list of user library directories
#   - for a device server, tango libraries directories are automatically appended
#   - '-L ../lib' is automatically appended in all cases
#

LIB_DIR_USER=



#=============================================================================
# LFLAGS_USR is the list of user link flags
#   - for a device server, tango libraries directories are automatically appended
#   - '-ldl -lpthread' is automatically appended in all cases
#
# !!! ATTENTION !!!
# Be aware that the order matters. 
# For example if you must link with libA, and if libA depends itself on libB
# you must use '-lA -lB' in this order as link flags, otherwise you will get
# 'undefined reference' errors
#
LFLAGS_USR=




#=============================================================================
# CXXFLAGS_USR lists the compilation flags specific for your library/device/exe
# This is the place where to put your compile-time macros using '-Dmy_macro'
#
CXXFLAGS_USR=



#
#	include Standard TANGO compilation options
#
include $(SOLEIL_ROOT)/env/tango.opt



#=============================================================================
# SVC_OBJS is the list of all objects needed to make the output
#
SVC_OBJS = 	$(OBJDIR)/AkimaInterpolator1D.o \
			$(OBJDIR)/BilinearInterpolator2D.o \
			$(OBJDIR)/CubicSplineInterpolator1D.o \
			$(OBJDIR)/FileReader.o \
			$(OBJDIR)/FileWriter.o \
			$(OBJDIR)/FourNearestNeighboursMeanInterpolator2D.o \
			$(OBJDIR)/GenericFileReader1D.o \
			$(OBJDIR)/GenericFileReader2D.o \
			$(OBJDIR)/InterpolationData1D.o \
			$(OBJDIR)/InterpolationData2D.o \
			$(OBJDIR)/Interpolator1D.o \
			$(OBJDIR)/Interpolator2D.o \
			$(OBJDIR)/Interpolator.o \
			$(OBJDIR)/LinearInterpolator1D.o \
			$(OBJDIR)/LowEnergyThetaBraggCorrectionReader.o \
			$(OBJDIR)/NearestNeighbourInterpolator2D.o \
			$(OBJDIR)/PeriodicAkimaInterpolator1D.o \
			$(OBJDIR)/PeriodicCubicSplineInterpolator1D.o \
			$(OBJDIR)/PolynomialInterpolator1D.o \
			$(OBJDIR)/SambaFileReader.o \
			$(OBJDIR)/Table1D.o \
			$(OBJDIR)/Table2D.o \
			$(OBJDIR)/Table.o \
			$(OBJDIR)/DataFile1D.o \
			$(OBJDIR)/DataFile2D.o \
			$(OBJDIR)/DataFile.o



#.SILENT:

#
# include common targets
#
include $(SOLEIL_ROOT)/env/common_target.opt
