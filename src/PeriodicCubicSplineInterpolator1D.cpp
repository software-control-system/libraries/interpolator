// PeriodicCubicSplineInterpolator1D.cpp: implementation of the PeriodicCubicSplineInterpolator1D class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "PeriodicCubicSplineInterpolator1D.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
PeriodicCubicSplineInterpolator1D::PeriodicCubicSplineInterpolator1D()
{

}

///Destructor
PeriodicCubicSplineInterpolator1D::~PeriodicCubicSplineInterpolator1D()
{
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
}

///This constructor requires 3 parameters :
///		- the name of the interpolator
///		- the description of the interpolator
///		- the interpolated data
PeriodicCubicSplineInterpolator1D::PeriodicCubicSplineInterpolator1D(	
				std::string sName, 
				std::string sDescription, 
				InterpolationData1D* mInterpolationData) : 
				Interpolator1D(sName,sDescription,"Periodic Cubic Spline",
				mInterpolationData)

{
	acc		= gsl_interp_accel_alloc ();
	spline	= gsl_spline_alloc (gsl_interp_cspline_periodic, getNbData());
	gsl_spline_init (spline, mInterpolationData->getXValues(), mInterpolationData->getYValues(), getNbData());
}

///Return the interpolated value for the wanted dValue
double PeriodicCubicSplineInterpolator1D::getInterpolatedValue(double dValue)
{
      return gsl_spline_eval (spline, dValue, acc);
}


///Method to update the interpolator according the new InterpolationData values
void PeriodicCubicSplineInterpolator1D::updateInterpolator()
{
	gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
	acc		= gsl_interp_accel_alloc ();
	spline	= gsl_spline_alloc (gsl_interp_cspline_periodic, getNbData());
	gsl_spline_init (spline, getInterpolatedData()->getXValues(), getInterpolatedData()->getYValues(), getNbData());
}

}

