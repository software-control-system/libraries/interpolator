// Table2D.cpp: implementation of the Table2D class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "Table2D.h"
#include "GenericFileReader2D.h"
#include "BilinearInterpolator2D.h"
#include "FourNearestNeighboursMeanInterpolator2D.h"
#include "NearestNeighbourInterpolator2D.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
/*Table2D::Table2D()
{

}*/

///Destructor
Table2D::~Table2D()
{
	DESTRUCTION(_mInterpolator);
	DESTRUCTION(_mData);
}

///This constructor requires 4 parameters :
///		- the name of the table
///		- the description of the table
///		- the wanted interpolation type (Bilinear,Nearest,others)
///		- the file path of the data
Table2D::Table2D(	std::string			sName,
					std::string			sDescription,
					std::string			sInterpolationType,	
					std::string			sFilePath) : 
Table(sName,sDescription,sInterpolationType,sFilePath)
{
	GenericFileReader2D*	_mFileReader = new GenericFileReader2D(sFilePath);
	
	double* mXValues = _mFileReader->getDataX();
	double* mYValues = _mFileReader->getDataY();	
	double* mZValues = _mFileReader->getDataZ();
	
	long lNbXValues = _mFileReader->getNbDataX();
	long lNbYValues = _mFileReader->getNbDataY();
//	long lNbZValues = _mFileReader->getNbDataZ();

	std::string _sXName = _mFileReader->getDataXName();
	std::string _sYName = _mFileReader->getDataYName();
	std::string _sZName = _mFileReader->getDataZName();
	
	_mData = new InterpolationData2D(	_sXName,_sYName,_sZName,
										lNbXValues,lNbYValues,
										mXValues,mYValues,mZValues);
	
	initializeInterpolator();
	delete(_mFileReader);

}


///This constructor requires 11 parameters :
///		- the name of the table
///		- the description of the table
///		- the wanted interpolation type ("Bilinear","Nearest","FourNearestMean")
///		- the name of the X variable
///		- the name of the Y variable
///		- the name of the Z variable 
///		- the number of X Data
///		- the number of Y Data
///		- the X values
///		- the Y values
///		- the Z values (unidimensional array)
Table2D::Table2D(	std::string			sName,
					std::string			sDescription,	
					std::string			sInterpolationType,
					std::string			sXName,
					std::string			sYName,
					std::string			sZName,
					long			lNbDataX,
					long			lNbDataY,
					double*			mXValues,
					double*			mYValues,
					double*			mZValues) :
Table(sName,sDescription,sInterpolationType)
{

	_mData = new InterpolationData2D(	sXName,sYName,sZName,
										lNbDataX,lNbDataY,
										mXValues,mYValues,mZValues);
	
	initializeInterpolator();


}

///Method to initialize the interpolator object according the wanted interpolation type
void Table2D::initializeInterpolator()
{
	///\todo FAIRE les differents cas d'interpolateurs avec un switch case !!!!
	if (getInterpolationType() == "Bilinear") 
	{
		_mInterpolator = new BilinearInterpolator2D("Bilinear Interpolator",
													getDescription(),
													_mData);
	}

	///\todo FAIRE les differents cas d'interpolateurs
	else if (getInterpolationType() == "Nearest") 
	{
		_mInterpolator = new NearestNeighbourInterpolator2D("Nearest Neighbour Interpolator",
															getDescription(),
															"Nearest Neighbour",
															_mData);
	}

	///\todo FAIRE les differents cas d'interpolateurs
	else if (getInterpolationType() == "FourNearestMean") 
	{
		_mInterpolator = new FourNearestNeighboursMeanInterpolator2D(	"FourNearestNeighboursMean Interpolator",
																		getDescription(),
																		_mData);
	}
}


///Return the output value according the entry points couple (dXValue,dYvalue) given in parameter
double Table2D::computeValue(double dXValue, double dYvalue)
{
	return _mInterpolator->getInterpolatedValue(dXValue,dYvalue);
}

///Fake method\n
///Only to provide the interface
double Table2D::computeValue()
{
	return 0.0;
}

///Return the InterpolationData2D object which contains all the data
InterpolationData2D* Table2D::getInterpolationData() const
{
	return _mData;
}

///Display info of the Table 2D\n Mainly used in DEBUG
void Table2D::printInfos()
{
	if(Interpolator::_verb == true) cout << "\n##### TABLE 2D --> " << getName() << " #####" << endl;
	if(Interpolator::_verb == true) cout << "\tInterpolation Type:" << getInterpolationType() << endl;
	if(Interpolator::_verb == true) cout << "\t" << getDescription() << endl;
}


/*double Table2D::computeValue()
{
	Monochromator* mMonochromator	= Monochromator::GetMonochromatorInstance();
	//std::string sEntryPointCorrection	= _mInterpolator->getInterpolatedData()->getXName();
 	Variable* mVariable				= mMonochromator->getVariable(_sEntryPoint);
	double dValue					= mVariable->getValue(); 
	return _mInterpolator->getInterpolatedValue(dValue);

  }
*/


///Method to modify the ith value of the X interpolation data structure with the dNewValue
void Table2D::setXValue(int i,double dNewValue)
{
	//First we update the interpolation data structure values
	_mInterpolator->getInterpolatedData()->setXValue(i,dNewValue);
}

///Method to modify the ith value of the Y interpolation data structure with the dNewValue
void Table2D::setYValue(int i,double dNewValue)
{
	//First we update the interpolation data structure values
	_mInterpolator->getInterpolatedData()->setYValue(i,dNewValue);
}

///Method to modify the ith value of the Z interpolation data structure with the dNewValue
void Table2D::setZValue(int i,int j,double dNewValue)
{
	//First we update the interpolation data structure values
	_mInterpolator->getInterpolatedData()->setZValue(i,j,dNewValue);
}

///Method to modify the values of the interpolation data structure with the dNewXValues, dNewYValues and dNewZValues
void Table2D::setValues(long lNbXData,long lNbYData,double* dNewXValues,double* dNewYValues,double* dNewZValues)
{
	//First we update the interpolation data structure values
	_mInterpolator->getInterpolatedData()->setValues(lNbXData,lNbYData,dNewXValues,dNewYValues,dNewZValues);
}



///Return the ith value of X array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the X array is null	
double Table2D::getXValue(int i) const // throw (IndexOutOfBoundException,NullPointerException)
{
	return _mInterpolator->getInterpolatedData()->getXValue(i);
}
	
///Return the ith value of Y array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the Y array is null	
double Table2D::getYValue(int i) const // throw (IndexOutOfBoundException,NullPointerException)
{
	return _mInterpolator->getInterpolatedData()->getYValue(i);
}

///Return the (ith,jth) value of Z array
///\exception This method throw a IndexOutOfBoundException exception if the i index is outside the bound
///\exception This method throw a NullPointerException exception if the Z array is null	
double Table2D::getZValue(int i,int j) const // throw (IndexOutOfBoundException,NullPointerException)
{
	return _mInterpolator->getInterpolatedData()->getZValue(i,j);
}

///Return the X values (first column)
///\exception This method throw a NullPointerException exception if the X array is null		
double* Table2D::getXValues() const // throw (NullPointerException)
{
	return _mInterpolator->getInterpolatedData()->getXValues();
}

///Return the Y values (second column)
///\exception This method throw a NullPointerException exception if the Y array is null	
double* Table2D::getYValues() const // throw (NullPointerException)
{
	return _mInterpolator->getInterpolatedData()->getYValues();
}

///Return the Z array
///\exception This method throw a NullPointerException exception if the Z array is null	
double* Table2D::getZValues() const // throw (NullPointerException)
{
	return _mInterpolator->getInterpolatedData()->getZValues();
}


///Return the number of X data
long Table2D::getNbXData() const
{
	return _mInterpolator->getInterpolatedData()->getNbXData();
}

///Return the number of Y data
long Table2D::getNbYData() const
{
	return _mInterpolator->getInterpolatedData()->getNbYData();
}

///Return the number of Z data
long Table2D::getNbZData() const
{
	return _mInterpolator->getInterpolatedData()->getNbZData();
}



}



