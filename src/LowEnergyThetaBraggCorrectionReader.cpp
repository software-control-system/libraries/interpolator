// LowEnergyThetaBraggCorrectionReader.cpp: implementation of the LowEnergyThetaBraggCorrectionReader class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "LowEnergyThetaBraggCorrectionReader.h"

#include <stdlib.h>
#include <stdio.h>
using namespace std;


namespace Interpolator
{

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
///Default constructor
LowEnergyThetaBraggCorrectionReader::LowEnergyThetaBraggCorrectionReader()
{

}
///Destructor
LowEnergyThetaBraggCorrectionReader::~LowEnergyThetaBraggCorrectionReader()
{

	if (_mEnergy)
	{
		delete [] _mEnergy;
		_mEnergy = 0;
	}

	if (_mThetaDiff)
	{
		delete [] _mThetaDiff;
		_mThetaDiff = 0;
	}
	if (_mFitDiff)
	{
		delete [] _mFitDiff;
		_mFitDiff = 0;
	}
	if (_mFWHMResolution)
	{
		delete [] _mFWHMResolution;
		_mFWHMResolution = 0;
	}

}

///This constructor requires 1 parameter :
///		- the name of the file to read
LowEnergyThetaBraggCorrectionReader::LowEnergyThetaBraggCorrectionReader(const std::string& sFileName) : FileReader(sFileName) 
{
	Parse();
}

///This constructor requires 3 parameters :
///		- the name of the file to read
///		- the number of columns of the file
///		- the number of lines of the file (without the first line which contains the name of the data)
LowEnergyThetaBraggCorrectionReader::LowEnergyThetaBraggCorrectionReader(const std::string& sFileName,long lNbColumns,long lNbLines) : FileReader(sFileName,lNbColumns,lNbLines)
{
	Parse();
}

///This method parse the buffer to read the low energy theta bragg correction\n
///The column 1 is the energy value variable column\n
///The column 2 is the theta diff variable column
///The column 3 is the fit diff variable column
///The column 4 is the FHWM resolution variable column
void LowEnergyThetaBraggCorrectionReader::Parse() // throw (ParseException)
{

int iNbData = getNbData();
if(Interpolator::_verb == true) cout << "NbData : " << iNbData << endl;

int iNbVariables = getNbVariables();
if(Interpolator::_verb == true) cout << "NbVariables : " << iNbVariables << endl;

//Reading of the first line to extract the variables names
std::string sColumnName;
 int i=0;

for (i=0;i<iNbVariables;i++) 
{
	sColumnName = extractString();
	if(Interpolator::_verb == true) cout << "Column " << i << " --> " << sColumnName << endl;
	
	switch (i) 
	{
	case 0 :
		_sEnergyName		= sColumnName;
		break;
	case 1 :
		_sThetaDiffName		= sColumnName;
		break;
	case 2 :
		_sFitDiffName		= sColumnName;
		break;
	case 3 :
		_sFWHMResolutionName= sColumnName;
		break;
	}
}

_mEnergy			= new double[iNbData];
_mThetaDiff			= new double[iNbData];
_mFitDiff			= new double[iNbData];
_mFWHMResolution	= new double[iNbData];
double dValue;

for (i=0;i<iNbData;i++) 
{
	for (int j=0;j<iNbVariables;j++) 
	{
		dValue = extractDouble();
			
		switch (j) 
		{
		case 0 :
			_mEnergy[i]			= dValue/1000.0; // On divise par 1000 car la table est fournie en eV
			break;
		case 1 :
			_mThetaDiff[i]		= DegresToRadians(dValue);
			break;
		case 2 :
			_mFitDiff[i]		= dValue;
			break;
		case 3 :
			_mFWHMResolution[i]	= dValue;
			break;
		default://Trow a ParseException !!!!
			///throw IndexOutOfBoundException("i",i,0,getNbVariables(),"LowEnergyThetaBraggCorrectionReader::getValue(int iColumn, int iIndex)",__FILE__,__LINE__);
			break;
		}
	}	
}

/*if(Interpolator::_verb == true) cout << "_mEnergy" <<endl;
for (int k=0;k<iNbDatas;k++)
{
	if(Interpolator::_verb == true) cout.precision(15);
	if(Interpolator::_verb == true) cout << k << " --> " << _mEnergy[k] << endl;
	
}
getchar();
if(Interpolator::_verb == true) cout << "_mThetaDiff" <<endl;
for (int l=0;l<iNbDatas;l++)
{ 
	if(Interpolator::_verb == true) cout.precision(15);
	if(Interpolator::_verb == true) cout << l << " --> " << (double)_mThetaDiff[l] << endl;
	
}
getchar();
if(Interpolator::_verb == true) cout << "_mFitDiff" <<endl;
for (int m=0;m<iNbDatas;m++)
{
	if(Interpolator::_verb == true) cout.precision(15);
	if(Interpolator::_verb == true) cout << m << " --> " << _mFitDiff[m] << endl;
	
}
getchar();	
if(Interpolator::_verb == true) cout << "_mFWHMResolution" <<endl;
for (int n=0;n<iNbDatas;n++)
{
	if(Interpolator::_verb == true) cout.precision(15);
	if(Interpolator::_verb == true) cout << n << " --> " << _mFWHMResolution[n] << endl;
	
}

getchar();
*/
}

///Return the energy values array.
///\exception This method throw a NullPointerException exception if the energy array is null
double* LowEnergyThetaBraggCorrectionReader::getEnergy() const // throw (NullPointerException)
{
	if (_mEnergy == 0) throw NullPointerException("_mEnergy","LowEnergyThetaBraggCorrectionReader::getEnergy()",__FILE__,__LINE__);
	return _mEnergy;
}

///Return the theta difference array to correct theta bragg value.
double* LowEnergyThetaBraggCorrectionReader::getThetaDiff() const // throw (NullPointerException)
{
	if (_mThetaDiff == 0) throw NullPointerException("_mThetaDiff","LowEnergyThetaBraggCorrectionReader::getThetaDiff()",__FILE__,__LINE__);
	return _mThetaDiff;
}

///Return the fit diff values array.
double* LowEnergyThetaBraggCorrectionReader::getFitDiff() const // throw (NullPointerException)
{
	if (_mFitDiff == 0) throw NullPointerException("_mFitDiff","LowEnergyThetaBraggCorrectionReader::getFitDiff()",__FILE__,__LINE__);
	return _mFitDiff;
}

///Return the FWHM resolution values array.
double* LowEnergyThetaBraggCorrectionReader::getFWHMResolution() const // throw (NullPointerException)
{
	if (_mFWHMResolution == 0) throw NullPointerException("_mFWHMResolution","LowEnergyThetaBraggCorrectionReader::getFWHMResolution()",__FILE__,__LINE__);
	return _mFWHMResolution;
}


///Return the ith value of the iColumn column array
///\exception This method returns a IndexOutOfBoundException if the iColumn is not found.
///\exception This method returns a IndexOutOfBoundException if the value with the ith index is not found.
double LowEnergyThetaBraggCorrectionReader::getValue(int iColumn, int iIndex) const // throw (IndexOutOfBoundException)
{	

	if ( (iIndex<0) || (iIndex>=getNbData())) throw IndexOutOfBoundException("iIndex",iIndex,0,getNbData(),"LowEnergyThetaBraggCorrectionReader::getValue(int iColumn, int iIndex)",__FILE__,__LINE__);
	
	switch (iColumn) 
	{
	case 0 :
		return _mEnergy[iIndex];
		break;
	case 1 :
		return _mThetaDiff[iIndex];
		break;
	case 2 :
		return _mFitDiff[iIndex];
		break;
	case 3 :
		return _mFWHMResolution[iIndex];
		break;
	default:
		throw IndexOutOfBoundException("iColumn",iColumn,0,getNbVariables(),"LowEnergyThetaBraggCorrectionReader::getValue(int iColumn, int iIndex)",__FILE__,__LINE__);
	}	
}

///Return the ith value of energy values array.
double LowEnergyThetaBraggCorrectionReader::getEnergyValue(int i) const
{
	return getValue(0,i);
}

///Return the ith value of theta difference array to correct theta bragg value.
double LowEnergyThetaBraggCorrectionReader::getThetaDiffValue(int i) const
{
	return getValue(1,i);
}

///Return the ith value of fit diff values array.
double LowEnergyThetaBraggCorrectionReader::getFitDiffValue(int i) const
{
	return getValue(2,i);
}

///Return the ith value of FHWM resolution values array.
double LowEnergyThetaBraggCorrectionReader::getFWHMResolutionValue(int i) const
{
	return getValue(3,i);
}

///Return the energy column name
std::string LowEnergyThetaBraggCorrectionReader::getEnergyName()			const
{
	return _sEnergyName;
}
///Return the theta diff column name
std::string LowEnergyThetaBraggCorrectionReader::getThetaDiffName()		const
{
	return _sThetaDiffName;
}
///Return the fit diff column name
std::string LowEnergyThetaBraggCorrectionReader::getFitDiffName()		const
{
	return _sFitDiffName;
}
///Return the FWHM resolution column name
std::string LowEnergyThetaBraggCorrectionReader::getFWHMResolutionName()	const
{
	return _sFWHMResolutionName;
}

}

