// LinearInterpolator1D.cpp: implementation of the LinearInterpolator1D class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "LinearInterpolator1D.h"

#include <iostream>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
LinearInterpolator1D::LinearInterpolator1D()
{

}

///Destructor
LinearInterpolator1D::~LinearInterpolator1D()
{
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
}

///This constructor requires 3 parameters :
///		- the name of the interpolator
///		- the description of the interpolator
///		- the interpolated data
LinearInterpolator1D::LinearInterpolator1D(	std::string sName, 
											std::string sDescription, 
											InterpolationData1D* mInterpolationData) : 
Interpolator1D(sName,sDescription,"Linear",mInterpolationData)

{
	acc		= gsl_interp_accel_alloc ();
	spline	= gsl_spline_alloc (gsl_interp_linear, getNbData());

	if(Interpolator::_verb == true) cout << "Values at init" << endl;
	for (long i=0;i<getNbData();i++) 
  {
    if(Interpolator::_verb == true) cout << i << " --> " << *(mInterpolationData->getXValues()+i) << " " << *(mInterpolationData->getYValues()+i) << endl;	
  }
	
	gsl_spline_init (spline, mInterpolationData->getXValues(), mInterpolationData->getYValues(), getNbData());
}

///Return the interpolated value for the wanted dValue
double LinearInterpolator1D::getInterpolatedValue(double dValue)
{
	if(Interpolator::_verb == true) cout << "Values used to interpolate : " << endl;
	for (unsigned int i=0;i<spline->size;i++)
  {
    if(Interpolator::_verb == true) cout << i << " --> " << *(spline->x+i) << " " << *(spline->y+i) << endl;
  }
	return gsl_spline_eval (spline, dValue, acc);
	  
}

///Method to update the interpolator according the new InterpolationData values
void LinearInterpolator1D::updateInterpolator()
{
	gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
	acc		= gsl_interp_accel_alloc ();
	spline	= gsl_spline_alloc (gsl_interp_linear, getNbData());
	gsl_spline_init (spline, getInterpolatedData()->getXValues(), getInterpolatedData()->getYValues(), getNbData());
}

}

