#include "DataFile1D.h"


namespace ICLIB {
	
DataFile1D::DataFile1D() {
	mFilename = "<unknown filename>";
}

DataFile1D::~DataFile1D() {
}

void DataFile1D::load(const std::string& filename) /*throw(Tango::DevFailed)*/ {
	mFilename = filename;
	clear();
	// read file
	std::ifstream file(filename.c_str());
	if(!file) {
		std::ostringstream err;
		err << "could not open file " << filename << " for reading";
		throw FileNotFoundException(filename,
									"DataFile1D::load",
									__FILE__,
									__LINE__);
		/*Tango::Except::throw_exception( 
			static_cast<const char*>("TANGO_DEVICE_ERROR"), 
			static_cast<const char*>(err.str().c_str()),
			static_cast<const char*>("DataFile1D::load"));*/
	}
	std::string line;
	std::vector< std::string > keys;
	if(std::getline(file,line)) {
		std::istringstream iline(line);
		std::string key;
		while(std::getline(iline,key,','))
			keys.push_back(key);
	}
	if(keys.size()<2) {
		std::ostringstream err;
		err << "error while parsing file " << filename << " : invalid header";
		throw ParseException (	filename,
								"DataFile1D::load",
								__FILE__,
								__LINE__);
		/*Tango::Except::throw_exception( 
			static_cast<const char*>("TANGO_DEVICE_ERROR"), 
			static_cast<const char*>(err.str().c_str()),
			static_cast<const char*>("DataFile2D::load"));*/
	}
	unsigned int numkey = 0;
	while(std::getline(file,line) && numkey<keys.size()) {
		std::vector< double > data;
		std::istringstream iline(line);
		double tmp;
		while(iline >> tmp)
			data.push_back(tmp);
		setValues(keys[numkey++],data);
	}
}

void DataFile1D::save(const std::string& filename) /*throw(Tango::DevFailed)*/ {
	std::ofstream file(filename.c_str(),ios::out);
	if(!file) {
		std::ostringstream err;
		err << "could not open file " << filename << " for writing";
		throw FileNotFoundException(filename,
									"DataFile1D::save",
									__FILE__,
									__LINE__);
		/*Tango::Except::throw_exception( 
			static_cast<const char*>("TANGO_DEVICE_ERROR"), 
			static_cast<const char*>(err.str().c_str()),
			static_cast<const char*>("DataFile1D::save"));*/
	}
	ValuesMap::iterator it=mValues.begin();
	while(it!=mValues.end()) {
		file << it->first;
		if(++it!=mValues.end())
			file << ",";
		else
			file << std::endl;
	}
	for(it = mValues.begin(); it!=mValues.end(); ++it) {
		std::vector< double >& data = it->second;
		unsigned long i=0;
		while(i<data.size()) {
			file << data[i];
			if(++i<data.size())
				file << "\t";
			else
				file << std::endl;
		}
	}
}

void DataFile1D::getArrayValues(vector<double>& aray,int& xdim,int& ydim) {
	xdim = 0;
	ydim = 0;
	if(mValues.size()>0) {
		xdim = mValues[0].second.size();
		ydim = mValues.size();
		aray.resize(xdim*ydim);
		for(int j=0; j<ydim; ++j) {
			for(int i=0; i<xdim; ++i) {
				aray[xdim*j+i] = mValues[j].second[i];
			}
		}
	}
}

void DataFile1D::setArrayValues(const vector<double>& aray,int xdim,int ydim) {
}

} // namespace
