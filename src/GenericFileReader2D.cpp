// GenericFileReader2D.cpp: implementation of the GenericFileReader2D class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "GenericFileReader2D.h"
#include "Tools.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
GenericFileReader2D::GenericFileReader2D()
{
	_mVector = new vector< double* >();
}

///Destructor
GenericFileReader2D::~GenericFileReader2D()
{
  for (int i=0;i< (signed)_mVector->size();i++) 
	{
		double * pColumn = getColumn(i);
		if (pColumn) 
		{
			delete [] pColumn;
			pColumn = 0;
		}
	}

	DESTRUCTION(_mVector);
}

///This constructor requires 1 parameter :
///		- the name of the file to read (the file must provide a header giving the number of column and the number of lines)
GenericFileReader2D::GenericFileReader2D(const std::string& sFileName) : FileReader(sFileName) 
{
	_mVector = new vector< double* >();
	Parse();
}

///This constructor requires 3 parameters :
///		- the name of the file to read
///		- the number of columns of the file
///		- the number of lines of the file (without the first line which contains the name of the data)	
GenericFileReader2D::GenericFileReader2D(const std::string& sFileName,long lNbColumns,long lNbLines) : FileReader(sFileName,lNbColumns,lNbLines)
{
	_mVector = new vector< double* >();
	Parse();
}


///This method parse the buffer to read a generic file\n
void GenericFileReader2D::Parse() // throw (ParseException)
{

int iNbLines = getNbData();
if(Interpolator::_verb == true) cout << "NbLines : " << iNbLines << endl;

int iNbColumns = getNbVariables();
if(Interpolator::_verb == true) cout << "NbColumns : " << iNbColumns << endl;

_lNbDataX = iNbLines;
_lNbDataY = iNbColumns;
_lNbDataZ = iNbLines*iNbColumns;


//Reading of the first line to extract the variables names
std::string sColumnName;

int i,j;

	
	_sXName = extractString();
	_sYName = extractString();
	_sZName = extractString();
	if(Interpolator::_verb == true) cout << "Name X --> " << _sXName << endl;
	if(Interpolator::_verb == true) cout << "Name Y --> " << _sYName << endl;
	if(Interpolator::_verb == true) cout << "Name Z --> " << _sZName << endl;


_mVector->push_back(new double[_lNbDataX]);
_mVector->push_back(new double[_lNbDataY]);
_mVector->push_back(new double[_lNbDataZ]);

//Columns 0 of the vector --> the X values
//Columns 1 of the vector --> the Y values
//Columns 2 of the vector --> the Z values (the matrix is stored in line like a vector.
double dValue;
//Extract the Y values and stores them in the column 1 (it begins at 0)

for (j=0;j<_lNbDataY;j++) 
{
		dValue = extractDouble();			
		((*_mVector)[1])[j] = dValue;
}


for (i=0;i<_lNbDataX;i++) 
{
	for (j=0;j<_lNbDataY+1;j++) 
	{
		dValue = extractDouble();			
		if (j == 0) 
		{
			((*_mVector)[0])[i] = dValue;
		}
		else 
		{
			((*_mVector)[2])[j+i*_lNbDataY-1] = dValue;	
		}
	}	
}

//displayAll();

}

///Return the X column name
std::string GenericFileReader2D::getDataXName()
{
	return _sXName;
}

///Return the Y line name
std::string GenericFileReader2D::getDataYName()
{
	return _sYName;
}

///Return the Z matrix name
std::string GenericFileReader2D::getDataZName()
{
	return _sZName;
}

///Return the X column data
double* GenericFileReader2D::getDataX()	const	
{
	return getColumn(0);
}

///Return the Y line data
double* GenericFileReader2D::getDataY()	const	
{
	return getColumn(1);
}

///Return the Z matrix data	
double* GenericFileReader2D::getDataZ()	const	
{
	return getColumn(2);
}

///Return the iColumnNumber column data (0 for X,1 for Y,2 for Z) 
///\exception This method throw a IndexOutOfBoundException exception if the iColumnNumber is outside the bound
///\exception This method throw a NullPointerException exception if the data vector is null	
double* GenericFileReader2D::getColumn(int iColumnNumber)	const	// throw (IndexOutOfBoundException,NullPointerException)
{
	if ((iColumnNumber < 0) || (iColumnNumber >= (int)_mVector->size())) throw IndexOutOfBoundException("iColumnNumber",iColumnNumber,0,getNbData(),"GenericFileReader2D::getColumn(int iColumnNumber)",__FILE__,__LINE__); 
	if (_mVector == 0) throw NullPointerException("_mVector","GenericFileReader2D::getColumn(int iColumnNumber)",__FILE__,__LINE__);
	return ((*_mVector)[iColumnNumber]);
}

///Return the number of X data	
long GenericFileReader2D::getNbDataX()
{
	return _lNbDataX;
}

///Return the number of Y data	
long GenericFileReader2D::getNbDataY()
{
	return _lNbDataY;
}

///Return the number of Z data	
long GenericFileReader2D::getNbDataZ()
{
	return _lNbDataZ;
}

///Display the data\n Used for DEBUG.
void GenericFileReader2D::displayAll()
{
	int k;
	for (int i=0;i<getNbDataX();i++)
	{
		for (int j=0;j<getNbDataY();j++)
		{
			k = i*_lNbDataY+j;
			if(Interpolator::_verb == true) cout << "(i=" << i << ",j=" << j << ",k=" << k << ")-->[" << getDataX()[i] << "," << getDataY()[j] << "," << getDataZ()[k] << "]" << endl;
		}
	}
	
}

}


