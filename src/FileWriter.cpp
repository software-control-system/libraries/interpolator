// FileWriter.cpp: implementation of the FileWriter class.
//
//////////////////////////////////////////////////////////////////////

#include "FileWriter.h"
#include "GenericFileReader1D.h"
#include <fstream>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
FileWriter::FileWriter()
{

}

FileWriter::~FileWriter()
{

}

FileWriter::FileWriter(const std::string& sPath,const std::string& sFileName,long lNbDatas, double* mXdata, double* mYdata)
{
	std::string sCompleteFilePath = sPath;
				sCompleteFilePath+=sFileName;
	
	std::ofstream fFile(sCompleteFilePath.c_str(),ios::out);
    
	for (int i=0;i<lNbDatas;i++)
	{
		fFile << mXdata[i] << "\t" << mYdata[i] << endl;
	}

	fFile.close();
}

FileWriter::FileWriter(const std::string& sCompleteFilePath,long lNbDatas, double* mXdata, double* mYdata)
{
	std::ofstream fFile(sCompleteFilePath.c_str(),ios::out);
    
	for (int i=0;i<lNbDatas;i++)
	{
		fFile << mXdata[i] << "\t" << mYdata[i] << endl;
	}

	fFile.close();
}

FileWriter::FileWriter(const std::string& sPath,
					   const std::string& sFileName,
					   long lIndex,
					   std::string& sExtension,
					   long lNbDatas, double* mXdata, double* mYdata)
{

	std::string sCompleteFilePath = sPath;
				sCompleteFilePath+="/";	
				sCompleteFilePath+=sFileName;	
				sCompleteFilePath+="_";	
				sCompleteFilePath+=ltos(lIndex);
				sCompleteFilePath+=".";	
				sCompleteFilePath+=sExtension;	

	std::ofstream fFile(sCompleteFilePath.c_str(),ios::out);
    
	for (int i=0;i<lNbDatas;i++)
	{
		fFile << mXdata[i] << "\t" << mYdata[i] << endl;
	}

	fFile.close();
}







FileWriter::FileWriter(const std::string& sCompleteFilePath, gsl_vector* mXdata, gsl_vector* mYdata)
{
	std::ofstream fFile(sCompleteFilePath.c_str(),ios::out);
	size_t lNbDatas = mXdata->size;

	for (unsigned int i=0;i<lNbDatas;i++)
	{
		fFile << gsl_vector_get(mXdata,i) << "\t" << gsl_vector_get(mYdata,i) << endl;
	}

	fFile.close();
}

FileWriter::FileWriter(const std::string& sPath,
					   const std::string& sFileName,
					   long lIndex,
					   const std::string& sExtension,
					   gsl_vector* mXdata, gsl_vector* mYdata)
{
	size_t lNbDatas = mXdata->size;
	std::string sCompleteFilePath = sPath;
				sCompleteFilePath+="/";	
				sCompleteFilePath+=sFileName;	
				sCompleteFilePath+="_";	
				sCompleteFilePath+=ltos(lIndex);
				sCompleteFilePath+=".";	
				sCompleteFilePath+=sExtension;	

	std::ofstream fFile(sCompleteFilePath.c_str(),ios::out);
    
	for (unsigned int i=0;i<lNbDatas;i++)
	{
		fFile << gsl_vector_get(mXdata,i) << "\t" << gsl_vector_get(mYdata,i) << endl;
	}

	fFile.close();
}



///For Swing theta theorical -> theta mechanical table updating
///Specific method must be 4 columns !!!
FileWriter::FileWriter(	const std::string& sCompleteFilePath,
						double dThetaTheorical,
						double dThetaMechanical,
						double dTolerance)
{
	
	GenericFileReader1D* mFile = new GenericFileReader1D(sCompleteFilePath);
  //- Get number of line in the file
	long	lNbData					= mFile->getNbData();
	
	std::ofstream fFile(sCompleteFilePath.c_str(),ios::out);
	fFile.precision(20);
	
  //- find the line associated to the theorical theta value and with a tolerance
	long lLineToModify = FindLine(mFile,dThetaTheorical,dTolerance);

	fFile << mFile->getNbVariables() << endl;	//Must be 4 !!!

	bool bAddingLine		= false;
	bool bInsertAtBeginning = false;
	
	if (lLineToModify == -1)
	{
		//bAddingLine = true;
		double dFirstValue = mFile->getValue(1,0);
		double dLastValue = mFile->getValue(1,lNbData-1);

		if ((dThetaTheorical > dFirstValue) && (dThetaTheorical < dLastValue))
		{
			//In this case no value must be inserted its only because the tolerance around the ThetaTheorical value can be checked.
			bAddingLine = false;
		}
		else
		{
			if (dFirstValue >= dThetaTheorical) 
			{
				bAddingLine			= true;
				bInsertAtBeginning	= true;
			}
			else
			{
				bAddingLine			= true;
				bInsertAtBeginning	= false;
			}
		}
	}
	
	double dOffset = dThetaMechanical - dThetaTheorical;

	//Generate a new file
	if (bAddingLine)
		fFile << (lNbData+1) << endl;
	else
		fFile << lNbData << endl;
	
	int i,j;
	for (i=0;i<mFile->getNbVariables();i++)
	{
		fFile << mFile->getColumnName(i) << "\t\t";
	}
	fFile << endl;
	
	if (bAddingLine && bInsertAtBeginning) 
	{
			fFile << RadiansToDegres(dThetaTheorical) << "\t\t";
			fFile << dThetaTheorical << "\t\t";
			fFile << dOffset << "\t\t";
			fFile << dThetaMechanical << "\n";
	}

	for (i=0;i<mFile->getNbData();i++)
	{
		if (i == lLineToModify)
		{
			fFile << RadiansToDegres(dThetaTheorical) << "\t\t";
			fFile << dThetaTheorical << "\t\t";
			fFile << dOffset << "\t\t";
			fFile << dThetaMechanical << "\n";
		}
		else
		{
			for (j=0;j<mFile->getNbVariables();j++)
			{
				fFile << mFile->getValue(j,i) << "\t\t";
			} 
			fFile << endl;
		}
	}

	if ((bAddingLine) && (!bInsertAtBeginning))
	{

		//We have to check if must be inserted in th

		fFile << RadiansToDegres(dThetaTheorical) << "\t\t";
		fFile << dThetaTheorical << "\t\t";
		fFile << dOffset << "\t\t";
		fFile << dThetaMechanical << "\n";
	}

	fFile.close();
}


long FileWriter::FindLine(GenericFileReader1D* mFile,double dThetaTheorical,double dTolerance)
{
	double* mTheoricalThetaColumn	= mFile->getColumn(1);
	long	lNbData					= mFile->getNbData();

	double dRangeMin	= dThetaTheorical - dTolerance;
	double dRangeMax	= dThetaTheorical + dTolerance;
	
	int i;
	
	for (i=0;i<lNbData;i++)
	{
		double dValueInColumn = mTheoricalThetaColumn[i];

		if (dRangeMin <= dValueInColumn && dValueInColumn <= dRangeMax)
		{
			return i;
		}
	}
	return -1;
}


}



