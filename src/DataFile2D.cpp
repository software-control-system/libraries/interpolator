#include "DataFile2D.h"
#include "iostream"

namespace ICLIB {

DataFile2D::DataFile2D() {
	mFilename = "<unknown filename>";
}

DataFile2D::~DataFile2D() {
}

void DataFile2D::load(const std::string& filename) /*throw(Tango::DevFailed) */{
	mFilename = filename;
	clear();
	std::ifstream file(filename.c_str());
	if(!file) {
		std::ostringstream err;
		err << "could not open file " << filename << " for reading";
		throw FileNotFoundException(filename,
									"DataFile2D::load",
									__FILE__,
									__LINE__);
		/*Tango::Except::throw_exception( 
			static_cast<const char*>("TANGO_DEVICE_ERROR"), 
			static_cast<const char*>(err.str().c_str()),
			static_cast<const char*>("DataFile2D::load"));*/
	}
	std::string line;
	std::vector< std::string > keys;
	if(std::getline(file,line)) {
		std::istringstream iline(line);
		std::string key;
		while(std::getline(iline,key,','))
			keys.push_back(key);
	}
	if(keys.size()!=3) {
		std::ostringstream err;
		err << "error while parsing file " << filename << " : invalid header";
		throw ParseException (	filename,
								"DataFile2D::load",
								__FILE__,
								__LINE__);
		/*Tango::Except::throw_exception( 
			static_cast<const char*>("TANGO_DEVICE_ERROR"), 
			static_cast<const char*>(err.str().c_str()),
			static_cast<const char*>("DataFile2D::load"));*/
	}
	// parse first line (xdata)
	if(std::getline(file,line)) {
		//cout << "\nline: " << line << endl;
		std::istringstream iline(line);
		//cout << "istringstream.str(): " << iline.str() << endl;
		double tmp;
		Values xdata;
		if(iline >> tmp)
		{
			//cout << "tmp x: " << tmp <<endl;
			xdata.push_back(tmp);
			while(iline >> tmp)
			{
				//cout << "tmp x: " << tmp <<endl;
				xdata.push_back(tmp);
			}
		}
		Values ydata;
		Values zdata;
		// parse next lines : ydata and zdata
		while(std::getline(file,line)) {
			//cout << "\nline: " << line << endl;
			std::istringstream iline(line);
			//cout << "istringstream.str(): " << iline.str() << endl;
			double tmp;
			if(iline >> tmp) 
			{
				//cout << "tmp y: " << tmp <<endl;
				ydata.push_back(tmp);
				while(iline >> tmp)
				{
					//cout << "tmp z: " << tmp <<endl;
					zdata.push_back(tmp);   
				}
			}
		}
		setValues(keys[0],xdata);
		setValues(keys[1],ydata);
		setValues(keys[2],zdata);
	}
}

void DataFile2D::save(const std::string& filename) /*throw(Tango::DevFailed)*/ {
	std::ofstream file(filename.c_str(),ios::out);
	if(!file) {
		std::ostringstream err;
		err << "could not open file " << filename << " for writing";
		throw FileNotFoundException(filename,
									"DataFile2D::save",
									__FILE__,
									__LINE__);
		/*Tango::Except::throw_exception( 
			static_cast<const char*>("TANGO_DEVICE_ERROR"), 
			static_cast<const char*>(err.str().c_str()),
			static_cast<const char*>("DataFile2D::save"));*/
	}
	if(mValues.size()==3) {
		// write header, get data
		ValuesMap::iterator it = mValues.begin();
		file << it->first;
		std::vector< double >& xdata = it->second;
		++it;
		file << "," << it->first;
		std::vector< double >& ydata = it->second;
		++it;
		file << "," << it->first;
		std::vector< double >& zdata = it->second;
		file << std::endl;

		// write xdata
		file << "0.0\t";
		unsigned long x=0;
		while(x<xdata.size()) {
			file << xdata[x];
			if(++x<xdata.size())
				file << "\t";
			else
				file << std::endl;
		}

		// write ydata and zdata
		for(unsigned long y=0; y<ydata.size(); ++y) {
			file << ydata[y] << "\t";
			x=0;
			while(x<xdata.size() && y*xdata.size()+x<zdata.size()) {
				file << zdata[y*xdata.size()+x];
				if(++x<xdata.size())
					file << "\t";
				else 
					file << std::endl;
			}
		}
	}
}

void DataFile2D::getArrayValues(vector<double>& aray,int& xdim,int& ydim) {
}

void DataFile2D::setArrayValues(const vector<double>& aray,int xdim,int ydim) {
}

} // namespace
