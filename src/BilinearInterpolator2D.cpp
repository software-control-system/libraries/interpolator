// BilinearInterpolator2D.cpp: implementation of the BilinearInterpolator2D class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32 
#pragma warning(disable:4786)
#endif
#include "BilinearInterpolator2D.h"

#include <iostream>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace Interpolator
{
///Default constructor
BilinearInterpolator2D::BilinearInterpolator2D()
{

}

///Destructor
BilinearInterpolator2D::~BilinearInterpolator2D()
{
}


///This constructor requires 3 parameters :
///		- the name of the interpolator
///		- the description of the interpolator
///		- the interpolation data
BilinearInterpolator2D::BilinearInterpolator2D(	std::string sName, 
												std::string sDescription, 
												InterpolationData2D* mInterpolationData) : 
Interpolator2D(sName,sDescription,"Bilinear",mInterpolationData)
{
}

///Return the interpolated value for the wanted couple (dXValue,dYvalue)
double BilinearInterpolator2D::getInterpolatedValue(double dXValue,double dYValue)
{
	double z = compute(dXValue,dYValue);
	return z;
}

///Compute the interpolated value
double BilinearInterpolator2D::compute(double dXValue,double dYValue)
{
	//Faire algo quicksort pour plus de rapidit� et faire les verifs sur les index calcul�s et sur le z si possible 
	int iXIndex = findXIndex(dXValue);
	int iYIndex = findYIndex(dYValue);
	
	double z,z1,z2,z3,z4,x1,x3,y1,y2;
	double a,b,c,d;

	//The 4 cases where the indexes for X and Y are outside the arrays (four corners cases)
	if ((iXIndex >= _mInterpolationData->getNbXData()) && (iYIndex >= _mInterpolationData->getNbYData()))
	{
		if(Interpolator::_verb == true) cout << "case 1" << endl;
		z = _mInterpolationData->getZValue(iXIndex-1,iYIndex-1);
	}
	
	else if ((iXIndex == -1) && (iYIndex == -1))
	{	
		if(Interpolator::_verb == true) cout << "case 2" << endl;
		z = _mInterpolationData->getZValue(0,0);
	}

	else if ((iXIndex == -1) && (iYIndex >= _mInterpolationData->getNbYData()))
	{
		if(Interpolator::_verb == true) cout << "case 3" << endl;
		z = _mInterpolationData->getZValue(0,iYIndex-1);
	}

	else if ((iXIndex >= _mInterpolationData->getNbXData()) && (iYIndex == -1))
	{
		if(Interpolator::_verb == true) cout << "case 4" << endl;
		z = _mInterpolationData->getZValue(iXIndex-1,0);
	}


	//The case when the X index is outside (bigger than the last index) of the array
	else if (iXIndex >= _mInterpolationData->getNbXData()) 
	{
		if(Interpolator::_verb == true) cout << "case 5" << endl;
		 x1	=  _mInterpolationData->getXValue(iXIndex-1);
		 
		 y1 =  _mInterpolationData->getYValue(iYIndex);
		 y2 =  _mInterpolationData->getYValue(iYIndex+1);

		 z1	= _mInterpolationData->getZValue(iXIndex-1,iYIndex);
		 z2	= _mInterpolationData->getZValue(iXIndex-1,iYIndex+1);		
		
		if(Interpolator::_verb == true) cout << "(iX-1="	 << iXIndex-1   << "|iY="   << iYIndex   << ")     --> (" << x1  << "," << y1  << "," << z1  << ")" << endl;
		if(Interpolator::_verb == true) cout << "(iX=" << iXIndex << "|iY+1=" << iYIndex+1 << ") --> (" << x1 << "," << y2 << "," << z2  << ")" << endl;

		double y1Moinsy2	= y1-y2;
				
		 a = (z2*y1 - z1*y2)/y1Moinsy2;
		 b = (z1 - z2)/y1Moinsy2;
		 
		 z = a + b*dYValue;
	}
	
	//The case when the X index is outside (lower than the first index) of the array
	else if (iXIndex == -1) 
	{
		if(Interpolator::_verb == true) cout << "case 6" << endl;
		 x1	=  _mInterpolationData->getXValue(iXIndex+1);
		 
		 y1 =  _mInterpolationData->getYValue(iYIndex);
		 y2 =  _mInterpolationData->getYValue(iYIndex+1);

		 z1	= _mInterpolationData->getZValue(iXIndex+1,iYIndex);
		 z2	= _mInterpolationData->getZValue(iXIndex+1,iYIndex+1);		
		
		if(Interpolator::_verb == true) cout << "(iX="	 << iXIndex   << "|iY="   << iYIndex   << ")     --> (" << x1  << "," << y1  << "," << z1  << ")" << endl;
		if(Interpolator::_verb == true) cout << "(iX+1=" << iXIndex+1 << "|iY+1=" << iYIndex+1 << ") --> (" << x1 << "," << y2 << "," << z2  << ")" << endl;

		double y1Moinsy2	= y1-y2;
				
		 a = (z2*y1 - z1*y2)/y1Moinsy2;
		 b = (z1 - z2)/y1Moinsy2;
		 
		 z = a + b*dYValue;
	}

	//The case when the Y index is outside (bigger than the last index) of the array
	else if (iYIndex >= _mInterpolationData->getNbYData())
	{
		if(Interpolator::_verb == true) cout << "case 7" << endl;
		 y1 =  _mInterpolationData->getYValue(iYIndex-1);

		 z1	= _mInterpolationData->getZValue(iXIndex,iYIndex-1);
		 z3	= _mInterpolationData->getZValue(iXIndex+1,iYIndex-1);		
	
		 x1	=  _mInterpolationData->getXValue(iXIndex);
		 x3	=  _mInterpolationData->getXValue(iXIndex+1);
		
		if(Interpolator::_verb == true) cout << "(iX="	 << iXIndex   << "|iY-1="   << iYIndex-1   << ")     --> (" << x1  << "," << y1  << "," << z1  << ")" << endl;
		if(Interpolator::_verb == true) cout << "(iX+1=" << iXIndex+1 << "|iY=" << iYIndex << ") --> (" << x3 << "," << y1 << "," << z3  << ")" << endl;

		double x3Moinsx1	= x3-x1;
				
		 a = (z1*x3 - z3*x1)/x3Moinsx1;
		 b = (z3 - z1)/x3Moinsx1;
		 
		 z = a + b*dXValue;
	}
	
		
	//The case when the Y index is outside (lower than the first index) of the array
	else if (iYIndex == -1 )
	{
		if(Interpolator::_verb == true) cout << "case 8" << endl;
		 y1 =  _mInterpolationData->getYValue(iYIndex+1);

		 z1	= _mInterpolationData->getZValue(iXIndex,iYIndex+1);
		 z3	= _mInterpolationData->getZValue(iXIndex+1,iYIndex+1);		
	
		 x1	=  _mInterpolationData->getXValue(iXIndex);
		 x3	=  _mInterpolationData->getXValue(iXIndex+1);
		
		if(Interpolator::_verb == true) cout << "(iX="	 << iXIndex   << "|iY="   << iYIndex   << ")     --> (" << x1  << "," << y1  << "," << z1  << ")" << endl;
		if(Interpolator::_verb == true) cout << "(iX+1=" << iXIndex+1 << "|iY+1=" << iYIndex+1 << ") --> (" << x3 << "," << y1 << "," << z3  << ")" << endl;

		double x3Moinsx1	= x3-x1;
				
		 a = (z1*x3 - z3*x1)/x3Moinsx1;
		 b = (z3 - z1)/x3Moinsx1;
		 
		 z = a + b*dXValue;
	}


	//Normal case
	else 
	{
		if(Interpolator::_verb == true) cout << "case 9" << endl;
		 z1	= _mInterpolationData->getZValue(iXIndex,iYIndex);
		 z2	= _mInterpolationData->getZValue(iXIndex,iYIndex+1);	
		 z3	= _mInterpolationData->getZValue(iXIndex+1,iYIndex+1);	
		 z4	= _mInterpolationData->getZValue(iXIndex+1,iYIndex);	
		
		 x1	=  _mInterpolationData->getXValue(iXIndex);
		 x3	=  _mInterpolationData->getXValue(iXIndex+1);
		 y1	=  _mInterpolationData->getYValue(iYIndex);
		 y2	=  _mInterpolationData->getYValue(iYIndex+1);	
		 
		if(Interpolator::_verb == true) cout << "(iX="	 << iXIndex   << "|iY="   << iYIndex   << ")     --> (" << x1  << "," << y1  << "," << z1  << ")" << endl;
		if(Interpolator::_verb == true) cout << "(iX+1=" << iXIndex+1 << "|iY="   << iYIndex   << ")   --> (" << x3 << "," << y1  << "," << z4  << ")" << endl;
		if(Interpolator::_verb == true) cout << "(iX+1=" << iXIndex+1 << "|iY+1=" << iYIndex+1 << ") --> (" << x3 << "," << y2 << "," << z3  << ")" << endl;
		if(Interpolator::_verb == true) cout << "(iX="   << iXIndex << "|iY+1="   << iYIndex+1 << ")   --> (" << x1  << "," << y2 << "," << z2  << ")" << endl;

		double x3Moinsx1	= x3-x1;
		double y2Moinsy1	= y2-y1;
		double dDeno    = x3Moinsx1*y2Moinsy1;
		
		
		 a = (x3*(y2*z1 - y1*z2)+x1*(y1*z3 - y2*z4))/dDeno;
		 b = (y1*(z2 - z3) + y2*(z4 - z1))/dDeno;
		 c = (x1*(z4 - z3) + x3*(z2 - z1))/dDeno;
		 d = (z1 - z2 +z3 - z4)/dDeno;
		 
		 z		= a + b*dXValue + c*dYValue + d*dXValue*dYValue;
	}
	if(Interpolator::_verb == true) cout << _mInterpolationData->getZName() << "=f(" << _mInterpolationData->getXName() << "," << _mInterpolationData->getYName() << ")=f("<< dXValue << "," << dYValue<< ")=" <<  z << endl;
	//if(Interpolator::_verb == true) cout << "z=f("<< dXValue << "," << dYValue<< ")=" <<  z << endl;
	return z;
}
}


