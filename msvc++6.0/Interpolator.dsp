# Microsoft Developer Studio Project File - Name="Interpolator" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=Interpolator - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Interpolator.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Interpolator.mak" CFG="Interpolator - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Interpolator - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "Interpolator - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Interpolator - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /MD /W3 /GR /GX /Z7 /O2 /I "$(SW_SUPPORT)\gsl\include" /I "$(SW_SUPPORT)\Interpolator\include" /I "$(SW_SUPPORT)\Monochromator\include" /I "$(SW_SUPPORT)\Utils\include" /I "$(SW_SUPPORT)\Exceptions\include" /D "WIN32" /D "NDEBUG" /D "INTEL86" /D "STRICT" /D "WIN32_LEAN_AND_MEAN" /D "VC_EXTRALEAN" /FR /FD /c
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\lib\static\msvc-6.0\libInterpolator.lib"

!ELSEIF  "$(CFG)" == "Interpolator - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /MDd /W3 /GR /GX /Z7 /Od /Gy /I "$(SW_SUPPORT)\gsl\include" /I "$(SW_SUPPORT)\Interpolator\include" /I "$(SW_SUPPORT)\Monochromator\include" /I "$(SW_SUPPORT)\Utils\include" /I "$(SW_SUPPORT)\Exceptions\include" /D "DEBUG" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "INTEL86" /D "STRICT" /D "WIN32_LEAN_AND_MEAN" /D "VC_EXTRALEAN" /FR /FD /GZ /c
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\lib\static\msvc-6.0\libInterpolatord.lib"

!ENDIF 

# Begin Target

# Name "Interpolator - Win32 Release"
# Name "Interpolator - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\src\AkimaInterpolator1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\BilinearInterpolator2D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\CubicSplineInterpolator1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\FileReader.cpp
# End Source File
# Begin Source File

SOURCE=..\src\FileWriter.cpp
# End Source File
# Begin Source File

SOURCE=..\src\FourNearestNeighboursMeanInterpolator2D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\GenericFileReader1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\GenericFileReader2D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\InterpolationData1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\InterpolationData2D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Interpolator.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Interpolator1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Interpolator2D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\LinearInterpolator1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\LowEnergyThetaBraggCorrectionReader.cpp
# End Source File
# Begin Source File

SOURCE=..\src\NearestNeighbourInterpolator2D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\PeriodicAkimaInterpolator1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\PeriodicCubicSplineInterpolator1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\PolynomialInterpolator1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\SambaFileReader.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Table.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Table1D.cpp
# End Source File
# Begin Source File

SOURCE=..\src\Table2D.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\include\AkimaInterpolator1D.h
# End Source File
# Begin Source File

SOURCE=..\include\BilinearInterpolator2D.h
# End Source File
# Begin Source File

SOURCE=..\include\CubicSplineInterpolator1D.h
# End Source File
# Begin Source File

SOURCE=..\include\FileReader.h
# End Source File
# Begin Source File

SOURCE=..\include\FileWriter.h
# End Source File
# Begin Source File

SOURCE=..\include\FourNearestNeighboursMeanInterpolator2D.h
# End Source File
# Begin Source File

SOURCE=..\include\GenericFileReader1D.h
# End Source File
# Begin Source File

SOURCE=..\include\GenericFileReader2D.h
# End Source File
# Begin Source File

SOURCE=..\include\InterpolationData1D.h
# End Source File
# Begin Source File

SOURCE=..\include\InterpolationData2D.h
# End Source File
# Begin Source File

SOURCE=..\include\Interpolator.h
# End Source File
# Begin Source File

SOURCE=..\include\Interpolator1D.h
# End Source File
# Begin Source File

SOURCE=..\include\Interpolator2D.h
# End Source File
# Begin Source File

SOURCE=..\include\LinearInterpolator1D.h
# End Source File
# Begin Source File

SOURCE=..\include\LowEnergyThetaBraggCorrectionReader.h
# End Source File
# Begin Source File

SOURCE=..\include\NearestNeighbourInterpolator2D.h
# End Source File
# Begin Source File

SOURCE=..\include\PeriodicAkimaInterpolator1D.h
# End Source File
# Begin Source File

SOURCE=..\include\PeriodicCubicSplineInterpolator1D.h
# End Source File
# Begin Source File

SOURCE=..\include\PolynomialInterpolator1D.h
# End Source File
# Begin Source File

SOURCE=..\include\SambaFileReader.h
# End Source File
# Begin Source File

SOURCE=..\include\Table.h
# End Source File
# Begin Source File

SOURCE=..\include\Table1D.h
# End Source File
# Begin Source File

SOURCE=..\include\Table2D.h
# End Source File
# End Group
# End Target
# End Project
