from conan import ConanFile

class interpolatorRecipe(ConanFile):
    name = "interpolator"
    version = "1.2.12"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Florent Langlois"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/interpolator"
    description = "Interpolator library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"

    def requirements(self):
        self.requires("gsl/1.11@soleil/stable")
        self.requires("utils/[>=1.0]@soleil/stable")
