#include <iostream>

#include <Table2D.h>

int main()
{
    long lNbDataX = 3;
    long lNbDataY = 4;
	
    double* mDataX = new double[lNbDataX];
    double* mDataY = new double[lNbDataY];
    double* mDataZ = new double[lNbDataX*lNbDataY];

    mDataX[0] = 1.0;
    mDataX[1] = 2.0;
    mDataX[2] = 3.0;

    mDataY[0] = 4.0;
    mDataY[1] = 5.0;
    mDataY[2] = 6.0;
    mDataY[3] = 7.0;

    mDataZ[0] = 8.0;
    mDataZ[1] = 9.0;
    mDataZ[2]  = 10.0;
    mDataZ[3]  = 11.0;
    mDataZ[4] = 12.0;
    mDataZ[5] = 13.0;
    mDataZ[6]  = 14.0;
    mDataZ[7]  = 15.0;
    mDataZ[8] = 16.0;
    mDataZ[9] = 17.0;
    mDataZ[10] = 18.0;
    mDataZ[11] = 19.0;
	

    Interpolator::Table2D* tableVinz = new Interpolator::Table2D("Table Vinz","Test","Bilinear","X","Y","Z",lNbDataX,lNbDataY,mDataX,mDataY,mDataZ);

    std::cout << tableVinz->computeValue() << '\n';


    return 0;
}
	
