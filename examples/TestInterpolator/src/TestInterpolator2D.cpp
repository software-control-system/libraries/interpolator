// TestInterpolator2D.cpp : Defines the entry point for the console application.
//

#include "GenericFileReader2D.h"
#include "Table2D.h"
#include "Table1D.h"
#include "Exception.h"

int main(int argc, char* argv[])
{
	
	Interpolator::GenericFileReader2D* mFileReader = new Interpolator::GenericFileReader2D("..\\data\\TempoRes.txt");//"D:\\Repository\\Tests\\TestInterpolator2D\\data\\TempoRes.txt");
	
	double* X = mFileReader->getDataX();
	double* Y = mFileReader->getDataY();
	double* Z = mFileReader->getDataZ();
	
	cout << mFileReader->getDataXName() << endl;
	for (int i=0;i<mFileReader->getNbDataX();i++)
	{
		cout << "X[" << i << "]= " << X[i] << endl;
	}

	cout << mFileReader->getDataYName() << endl;
	for (int j=0;j<mFileReader->getNbDataY();j++)
	{
		cout << "Y[" << j << "]= " << Y[j] << endl;
	}
	cout << mFileReader->getDataZName() << endl;
	for (int k=0;k<mFileReader->getNbDataZ();k++)
	{
		cout << "Z[" << k << "]= " << Z[k] << endl;
	}



	Interpolator::Table2D* mTable = new Interpolator::Table2D(	"Res=f(E,fs)",
									"Interpolation of the resolution",
									"Bilinear",
									"..\\data\\TempoResolution350AFe10.txt");
	cout << mTable->getInterpolationData()->getXName() << endl;
	cout << mTable->getInterpolationData()->getYName() << endl;
	cout << mTable->getInterpolationData()->getZName() << endl;
	
	

	cout << "INTERPOLATION --> " << mTable->getInterpolationType() << endl;
	//Cas o� les 2 indexes ne sont pas dans le tableau
	mTable->computeValue(3,30);
	cout << endl;

	mTable->computeValue(700,30);
		cout << endl;
	
		
		mTable->computeValue(3,130.0);
		cout << endl;
	mTable->computeValue(700,130.0);
		cout << endl;

	//Cas o� 1 des 2 indexes n'est pas dans le tableau
	mTable->computeValue(3,55.0);
		cout << endl;
	mTable->computeValue(700,55.0);
		cout << endl;
	
	mTable->computeValue(27.5,30.0);
		cout << endl;
	mTable->computeValue(27.5,130.0);
		cout << endl;

	//Les 2 indexes sont bons
	mTable->computeValue(70.0,45.5);
	
	Interpolator::Table1D* _mGrooveDepthGrating1Table;


	try
	{

	_mGrooveDepthGrating1Table	= new Interpolator::Table1D("Table 1" ,
													"X->Depth"	,
													"Linear",
													"..\\data\\TEMPO350Transl_Depth.txt",
													0,
													1);
	}
	catch (Exception& e)
	{
		cout << e.getReason() << endl;
		cout << e.getDescription() << endl;
		cout << e.getOrigin() << endl;
	}

	Interpolator::Table1D* _mGrooveDepthGrating2Table;

	try
	{

	_mGrooveDepthGrating2Table	= new Interpolator::Table1D("Table 2" ,
												"X->Depth"	,
												"Linear",
												"..\\data\\TEMPO800Transl_Depth.txt",
													0,
													1);
	}
	catch (Exception& e)
	{
		cout << e.getReason() << endl;
		cout << e.getDescription() << endl;
		cout << e.getOrigin() << endl;
	}


	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getNbData() << endl;
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getXName() << endl;
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getYName() << endl;
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getXValue(2) << endl;
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getYValue(3) << endl;
	
	_mGrooveDepthGrating2Table->getInterpolationData()->setXValue(2,12.6);
	_mGrooveDepthGrating2Table->getInterpolationData()->setYValue(3,24.6);
	
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getXValue(2) << endl;
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getYValue(3) << endl;
	
	double* newDataX = new double[3];
	double* newDataY = new double[3];

	newDataX[0] = 1.0;	newDataX[1] = 2.0;	newDataX[2] = 3.0;
	newDataY[0] = 4.0;	newDataY[1] = 5.0;	newDataY[2] = 6.0;

	_mGrooveDepthGrating2Table->getInterpolationData()->setValues(3,newDataX,newDataY);

	delete(newDataX);
	delete(newDataY);
	
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getNbData() << endl;
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getXName() << endl;
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getYName() << endl;
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getXValue(2) << endl;
	cout << _mGrooveDepthGrating2Table->getInterpolationData()->getYValue(1) << endl;

	


	Interpolator::Table2D* mTable2 = new Interpolator::Table2D(	"Res=f(E,fs)",
									"Interpolation of the resolution",
									"Nearest",
									"..\\data\\TempoResolution350AFe10.txt");
	cout << mTable2->getInterpolationData()->getXName() << endl;
	cout << mTable2->getInterpolationData()->getYName() << endl;
	cout << mTable2->getInterpolationData()->getZName() << endl;
	
	
	cout << "\nINTERPOLATION --> " << mTable2->getInterpolationType() << endl;
	//Cas o� les 2 indexes ne sont pas dans le tableau
	mTable2->computeValue(3,30);cout << endl;
	mTable2->computeValue(700,30);cout << endl;
	mTable2->computeValue(3,130.0);cout << endl;
	mTable2->computeValue(700,130.0);cout << endl;

	//Cas o� 1 des 2 indexes n'est pas dans le tableau
	mTable2->computeValue(3,55.0);cout << endl;
	mTable2->computeValue(700,55.0);cout << endl;
	mTable2->computeValue(27.5,30.0);cout << endl;
	mTable2->computeValue(27.5,130.0);cout << endl;

	//Les 2 indexes sont bons
	mTable2->computeValue(70.0,45.5);
	
	Interpolator::Table2D* mTable3 = new Interpolator::Table2D(	"Res=f(E,fs)",
									"Interpolation of the resolution","FourNearestMean",
									"..\\data\\TempoResolution350AFe10.txt");
	cout << mTable3->getInterpolationData()->getXName() << endl;
	cout << mTable3->getInterpolationData()->getYName() << endl;
	cout << mTable3->getInterpolationData()->getZName() << endl;
	
	cout << "\nINTERPOLATION --> " << mTable3->getInterpolationType() << endl;
	//Cas o� les 2 indexes ne sont pas dans le tableau
	mTable3->computeValue(3,30);cout << endl;
	mTable3->computeValue(700,30);cout << endl;
	mTable3->computeValue(3,130.0);cout << endl;
	mTable3->computeValue(700,130.0);cout << endl;

	//Cas o� 1 des 2 indexes n'est pas dans le tableau
	mTable3->computeValue(3,55.0);cout << endl;
	mTable3->computeValue(700,55.0);cout << endl;
	mTable3->computeValue(27.5,30.0);cout << endl;
	mTable3->computeValue(27.5,130.0);cout << endl;

	//Les 2 indexes sont bons
	mTable3->computeValue(70.0,45.5);


	long lNbDataX = 3;
	long lNbDataY = 4;
	
	double* mDataX = new double[lNbDataX];
	double* mDataY = new double[lNbDataY];
	double* mDataZ = new double[lNbDataX*lNbDataY];

	mDataX[0] = 1.0;	mDataX[1] = 2.0;	mDataX[2] = 3.0;
	mDataY[0] = 4.0;	mDataY[1] = 5.0;	mDataY[2] = 6.0; mDataY[3] = 7.0;

	mDataZ[0] = 8.0;	mDataZ[1] = 9.0;	mDataZ[2]  = 10.0; mDataZ[3]  = 11.0;
	mDataZ[4] = 12.0;	mDataZ[5] = 13.0;	mDataZ[6]  = 14.0; mDataZ[7]  = 15.0;
	mDataZ[8] = 16.0;	mDataZ[9] = 17.0;	mDataZ[10] = 18.0; mDataZ[11] = 19.0;
	

	Interpolator::Table2D* tableVinz = new Interpolator::Table2D("Table Vinz","Test","Bilinear","X","Y","Z",lNbDataX,lNbDataY,mDataX,mDataY,mDataZ);

	tableVinz->getInterpolationData()->printInfos();
	cout << endl;
	cout << tableVinz->getInterpolationData()->getNbXData() << endl;
	cout << tableVinz->getInterpolationData()->getNbYData() << endl;
	cout << tableVinz->getInterpolationData()->getNbZData() << endl;
	cout << tableVinz->getInterpolationData()->getXName() << endl;
	cout << tableVinz->getInterpolationData()->getYName() << endl;
	cout << tableVinz->getInterpolationData()->getZName() << endl;
	cout << tableVinz->getInterpolationData()->getXValue(2) << endl;
	cout << tableVinz->getInterpolationData()->getYValue(3) << endl;
	cout << tableVinz->getInterpolationData()->getZValue(2,3) << endl;
	

	cout << "\nINTERPOLATION --> " << tableVinz->getInterpolationType() << endl;
	//Cas o� les 2 indexes ne sont pas dans le tableau
	tableVinz->computeValue(0.0,3.0);cout << endl;
	tableVinz->computeValue(4.0,3.0);cout << endl;
	tableVinz->computeValue(4.0,8.0);cout << endl;
	tableVinz->computeValue(0.0,8.0);cout << endl;

	//Cas o� 1 des 2 indexes n'est pas dans le tableau
	tableVinz->computeValue(1.0,3.0);cout << endl;
	tableVinz->computeValue(4.0,4.0);cout << endl;
	tableVinz->computeValue(4.0,7.0);cout << endl;
	tableVinz->computeValue(1.0,8.0);cout << endl;

	//Les 2 indexes sont bons
	tableVinz->computeValue(2.5,5.5);


	cout << endl;

	tableVinz->getInterpolationData()->setXValue(2,12.6);
	tableVinz->getInterpolationData()->setYValue(3,24.6);
	tableVinz->getInterpolationData()->setZValue(2,3,34.6);

	cout << tableVinz->getInterpolationData()->getXValue(2) << endl;
	cout << tableVinz->getInterpolationData()->getYValue(3) << endl;
	cout << tableVinz->getInterpolationData()->getZValue(2,3) << endl;

	tableVinz->getInterpolationData()->printInfos();

	delete(_mGrooveDepthGrating1Table);
	delete(_mGrooveDepthGrating2Table);
	
	delete(mTable);
	delete(mTable2);
	delete(mTable3);
	delete(mFileReader);




	return 0;
}

