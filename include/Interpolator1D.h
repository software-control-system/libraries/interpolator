// Interpolator1D.h: interface for the Interpolator1D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_Interpolator1D_H__A7A7A0C7_4ED6_4E13_A70E_97DD503C5DB7__INCLUDED_)
#define AFX_Interpolator1D_H__A7A7A0C7_4ED6_4E13_A70E_97DD503C5DB7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Interpolator.h"
#include <string>
//using namespace std;
#include "InterpolationData1D.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
namespace Interpolator
{
///This class provide an interface for a 1D interpolator
class Interpolator1D : public Interpolator
{
public:

	Interpolator1D();
	Interpolator1D(	std::string sName, 
					std::string sDescription, 
					std::string sInterpolationType,
					InterpolationData1D* mInterpolationData);
	
	virtual ~Interpolator1D();

	long	getNbData();
	virtual double getInterpolatedValue(double dValue)=0;
	virtual InterpolationData1D* getInterpolatedData();

	virtual void updateInterpolator()=0;

protected:
	gsl_interp_accel	*acc;
	gsl_spline			*spline;

private:
	InterpolationData1D* _mInterpolationData;

	
};
}
#endif // !defined(AFX_Interpolator1D_H__A7A7A0C7_4ED6_4E13_A70E_97DD503C5DB7__INCLUDED_)
