// BilinearInterpolator2D.h: interface for the BilinearInterpolator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BILINEARINTERPOLATOR_H__251D6F0F_36FE_43A6_A92D_584B186C7A88__INCLUDED_)
#define AFX_BILINEARINTERPOLATOR_H__251D6F0F_36FE_43A6_A92D_584B186C7A88__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Interpolator2D.h"
#include "InterpolationData2D.h" 

namespace Interpolator
{
///This class performs a Bilinear interpolation
class BilinearInterpolator2D : public Interpolator2D  
{
public:
	BilinearInterpolator2D();
	virtual ~BilinearInterpolator2D();

	BilinearInterpolator2D(	std::string sName, 
							std::string sDescription,
							InterpolationData2D* mInterpolationData);

	virtual double getInterpolatedValue(double dXValue,double dYValue);
private:
	double compute(double dXValue,double dYValue);

};
}
#endif // !defined(AFX_BILINEARINTERPOLATOR_H__251D6F0F_36FE_43A6_A92D_584B186C7A88__INCLUDED_)
