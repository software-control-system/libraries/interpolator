// Table1D.h: interface for the Table1D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABLE1D_H__F1D3EADA_899F_4335_95AC_21EE6135F5A9__INCLUDED_)
#define AFX_TABLE1D_H__F1D3EADA_899F_4335_95AC_21EE6135F5A9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "ComputingObject.h"

#include <string>
//using namespace std;
#include "Interpolator1D.h"

#include "GenericFileReader1D.h"
#include "InterpolationData1D.h"
#include "Table.h"

#include "Variable.h"

namespace Interpolator
{
///This class is used to manage a 1D table.
///If you use the constructor with data (double*) it is necessary to delete the objects pointed.
///Indeed during the creation of the table, the data are copied in an internal structure.
///\todo Template pour le type de donn�es � interpol�es (double -> Type)
class Table1D : public Table  
{
public:
	//Table1D();
	virtual ~Table1D();

	Table1D(std::string			sName,
			std::string			sDescription,	
			std::string			sEntryPoint, 
			std::string			sOutputPoint,
			Variable*			mEntryPoint,
			Interpolator1D*		mInterpolator1D/*,
			Interpolator1D*		mInverseInterpolator1D*/);

	Table1D(std::string			sName,
			std::string			sDescription,	
			std::string			sEntryPoint, 
			std::string			sOutputPoint,
			Variable*			mEntryPoint,
			std::string			sInterpolationType,
			long				lNbData,
			double*				mX,
			double*				mY);
	
	Table1D(std::string			sName,
			std::string			sDescription,	
			std::string			sInterpolationType,
			std::string			sFilePath,
			int					iFirstColumn,
			int					iSecondColumn);


	virtual double computeValue(double dValue);
	virtual double computeValue();

	std::string		getEntryPoint() const;
	std::string		getOutputPoint() const;
	Variable*		getEntryPointVariable() const;

	virtual Interpolator1D* getInterpolator() const;	
	virtual InterpolationData1D* getInterpolationData() const;

	virtual void printInfos();

	void setXValue(int i,double dNewValue); 
	void setYValue(int i,double dNewValue); 
	void setValues(long lNbData,double* dNewXValues,double* dNewYValues);

	double	getXValue(int i) const;
	double	getYValue(int i) const;	
	double* getXValues() const;
	double* getYValues() const;

	long	getNbData() const;

	void	checkDataAreCorrects();

	void	WriteTable();

private:

	std::string				_sEntryPoint;
	std::string				_sOutputPoint;
	Variable*				_mEntryPoint;

	Interpolator1D*			_mInterpolator;
	InterpolationData1D*	_mData;


	//Variable*		_mOutputPoint; Normalement pas utile pour l'instant. Idem mieux vaut passer par la chaine
	//car la variable d�pend du slot courant. Donc soit on cr�e 2 objets Table1Ds pour la interpolation juste pour avoir
	//les objets variables des 2 slots par exmple Tz2Slot1 / Tz2Slot2 passe en param�tre de 2 Table1Ds !
	//Ca duplique mais au moins ca �vite de faire des comparaisons de chaines risqu�es !!!

//	GenericFileReader1D*	_mFileReader;

};
}
#endif // !defined(AFX_TABLE1D_H__F1D3EADA_899F_4335_95AC_21EE6135F5A9__INCLUDED_)
