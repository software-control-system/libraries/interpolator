// GenericFileReader2D.h: interface for the GenericFileReader2D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GenericFileReader2D_H__BEB897D3_E13B_4274_AC5D_DC1A5995A51B__INCLUDED_)
#define AFX_GenericFileReader2D_H__BEB897D3_E13B_4274_AC5D_DC1A5995A51B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FileReader.h"
#include <vector>
#include "IndexOutOfBoundException.h"
#include "NullPointerException.h"
#include "ParseException.h"
//using namespace std;

namespace Interpolator
{
///This class is a generic 2D file reader (for data sorted like a matrix)
class GenericFileReader2D : public FileReader  
{
public:

	GenericFileReader2D();
	virtual ~GenericFileReader2D();

	GenericFileReader2D(const std::string& sFileName);
	GenericFileReader2D(const std::string& sFileName,long lNbColumns,long lNbLines);

	virtual void Parse(); // throw (ParseException);

	double* getDataX()					const;	
	double* getDataY()					const;
	double* getDataZ()					const;
	double	getValue(int iColumnNumber, int iLineNumber)	const;	// throw (IndexOutOfBoundException);

	long getNbDataX();
	long getNbDataY();
	long getNbDataZ();
	
	std::string getDataXName();
	std::string getDataYName();
	std::string getDataZName();
	void displayAll();
private:
	
	double* getColumn(int iColumnNumber) const; // throw (IndexOutOfBoundException,NullPointerException);	

	long				_lNbDataX;
	long				_lNbDataY;
	long				_lNbDataZ;
	
	std::string			_sXName;
	std::string			_sYName;
	std::string			_sZName;

	vector<double*> *	_mVector;
};

}

#endif // !defined(AFX_GenericFileReader2D_H__BEB897D3_E13B_4274_AC5D_DC1A5995A51B__INCLUDED_)
