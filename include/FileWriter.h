// FileWriter.h: interface for the FileWriter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILEWRITER_H__3BD13DC0_CE2D_4FA4_991A_2B1C637F1686__INCLUDED_)
#define AFX_FILEWRITER_H__3BD13DC0_CE2D_4FA4_991A_2B1C637F1686__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string> 
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>

#include "FileNotFoundException.h"
#include "ParseException.h"
#include <gsl/gsl_vector.h>

#include "GenericFileReader1D.h"
namespace Interpolator
{
///This class provides basics tools to write files\n
///It is mainly used for the correcting and coupling files\n
///Thanks to this abstract class, it is possible to define a specific file writer for each scientific group\n
///The only things to do is to implement the Write method.
class FileWriter  
{
public:
	FileWriter();
	FileWriter(const std::string& sPath,const std::string& sFileName);									// throw (FileNotFoundException);
	FileWriter(const std::string& sPath,const std::string& sFileName,long lNbVariables,long lNbDatas);	// throw (FileNotFoundException);

	FileWriter(const std::string& sPath,const std::string& sFileName,long lNbDatas, double* mXdata, double* mYdata);	
	FileWriter(const std::string& sCompleteFilePath,long lNbDatas, double* mXdata, double* mYdata);
	FileWriter(const std::string& sPath,
					   const std::string& sFileName,
					   long lIndex,
					   std::string& sExtension,
					   long lNbDatas, double* mXdata, double* mYdata);


	FileWriter(const std::string& sCompleteFilePath,gsl_vector* mXdata, gsl_vector* mYdata);
	FileWriter(const std::string& sPath,
					   const std::string& sFileName,
					   long lIndex,
					   const std::string& sExtension,
					   gsl_vector* mXdata, gsl_vector* mYdata);


	FileWriter(	const std::string& sCompleteFilePath,
				double dThetaTheorical,
				double dThetaMechanical,
				double dTolerance);

	long FindLine(GenericFileReader1D* mFile,double dThetaTheorical,double dTolerance);

	bool isWritable();

	virtual ~FileWriter();

	double			writeDouble();
	std::string		writeString() ;
	int				writeInt();
	std::string		writeLine();
	
	long	extractFileSize();		// throw (FileNotFoundException);	
	long	extractNbFileLines();	// throw (FileNotFoundException);
	long	extractBufferSize();
//	long	extractNbData();
//	long	extractNbVariables();

	long	getFileSize()		const;
	long	getNbFileLines()	const;
	long	getBufferSize()		const;
	long	getNbVariables()	const;
	long	getNbData()			const;
	
	void	setFileSize		(long lFileSize);	
	void	setNbFileLines	(long lNbFileLines);
	void	setBufferSize	(long lBufferSize);
	void	setNbVariables	(long lNbVariables);
	void	setNbData		(long lNbData);

	void	copyFileToBuffer(); // throw (FileNotFoundException);
	
	//virtual void Write(); // throw (ParseException)=0;
	
	void	extractHeader();

private:
	std::string			_sFileName;
	
	long				_lNbVariables;
	long				_lNbData;

	std::stringstream	_mFileBuffer;
	
	long				_lBufferSize;	
	long				_lFileSize;
	long				_lNbFileLines;
};
}
#endif // !defined(AFX_FILEWRITER_H__3BD13DC0_CE2D_4FA4_991A_2B1C637F1686__INCLUDED_)
