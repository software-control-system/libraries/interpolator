// Interpolator.h: interface for the Interpolator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INTERPOLATOR_H__D63FBE88_0D3E_4FBB_B4BB_ABF88A25F8CE__INCLUDED_)
#define AFX_INTERPOLATOR_H__D63FBE88_0D3E_4FBB_B4BB_ABF88A25F8CE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/*#ifdef VERBOSE
  _verb = true;
#endif*/

#include <string>
//using namespace std;
namespace Interpolator
{
///This class provide an interface for an interpolator
class Interpolator  
{
public:
	static bool _verb;
	static void setVerbose(bool verb = false) {_verb = verb;};

	Interpolator();
	virtual ~Interpolator();

	Interpolator(	std::string sName, 
					std::string sDescription, 
					std::string sInterpolationType);

	std::string	getName() const;
	std::string	getDescription() const;
	std::string	getInterpolationType() const;
	
private :
	std::string _sName;
	std::string _sDescription;
	std::string _sInterpolationType;
};
}
#endif // !defined(AFX_INTERPOLATOR_H__D63FBE88_0D3E_4FBB_B4BB_ABF88A25F8CE__INCLUDED_)
