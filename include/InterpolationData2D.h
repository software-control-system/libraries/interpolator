// InterpolationData2D.h: interface for the InterpolationData2D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_2DINTERPOLATIONDATA_H__AEBA8863_48FA_4871_B822_A31645EDC6C9__INCLUDED_)
#define AFX_2DINTERPOLATIONDATA_H__AEBA8863_48FA_4871_B822_A31645EDC6C9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>
//using namespace std;

#include "IndexOutOfBoundException.h"
#include "NullPointerException.h"
#include "Interpolator.h"

namespace Interpolator
{
///This class stores 2D data for a 2D interpolator
class InterpolationData2D  
{
public:

	InterpolationData2D();
	InterpolationData2D(std::string sXName, std::string sYName, std::string sZName,long lNbXData,long lNbYData,double* mXValues, double* mYValues,double* mZValues);
	virtual ~InterpolationData2D();

	std::string getXName() const;
	std::string getYName() const;
	std::string getZName() const;

	double getXValue(int i) const ;// throw (IndexOutOfBoundException,NullPointerException);
	double getYValue(int i) const ;// throw (IndexOutOfBoundException,NullPointerException);
	double getZValue(int i,int j) const ;// throw (IndexOutOfBoundException,NullPointerException);
	
	double* getXValues() const ;// throw (NullPointerException);
	double* getYValues() const ;// throw (NullPointerException);
	double* getZValues() const ;// throw (NullPointerException);
	
	void setXValue(int i,double dNewValue) ;// throw (IndexOutOfBoundException,NullPointerException);;
	void setYValue(int i,double dNewValue) ;// throw (IndexOutOfBoundException,NullPointerException);;
	void setZValue(int i,int j,double dNewValue) ;// throw (IndexOutOfBoundException,NullPointerException);;
		
	void setValues(long lNbXData,long lNbYData,double* dNewXValues,double* dNewYValues,double* dNewZValues);	
	
	long getNbXData() const;
	long getNbYData() const;
	long getNbZData() const;

	virtual void printInfos();

private:	
	void setXValues(double* dNewValues);
	void setYValues(double* dNewValues);
	void setZValues(double* dNewValues);	
	
	std::string		_sXName;
	std::string		_sYName;
	std::string		_sZName;
	
	long			_lNbXData;
	long			_lNbYData;
	long			_lNbZData;

	double*			_mXValues;
	double*			_mYValues;
	double*			_mZValues;
};
}
#endif // !defined(AFX_2DINTERPOLATIONDATA_H__AEBA8863_48FA_4871_B822_A31645EDC6C9__INCLUDED_)
