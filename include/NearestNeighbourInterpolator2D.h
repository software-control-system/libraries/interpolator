// NearestNeighbourInterpolator2D.h: interface for the NearestNeighbourInterpolator2D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NEARESTNEIGHBOURINTERPOLATOR2D_H__B66CF9B5_666E_4B14_8DCF_D358B549AF3B__INCLUDED_)
#define AFX_NEARESTNEIGHBOURINTERPOLATOR2D_H__B66CF9B5_666E_4B14_8DCF_D358B549AF3B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Interpolator2D.h"
namespace Interpolator
{
///This class performs a nearest neighbour interpolation
class NearestNeighbourInterpolator2D : public Interpolator2D  
{
public:
	NearestNeighbourInterpolator2D();
	virtual ~NearestNeighbourInterpolator2D();
	NearestNeighbourInterpolator2D(	std::string sName, 
									std::string sDescription,
									std::string sInterpolationType,
									InterpolationData2D* mInterpolationData);

	virtual double getInterpolatedValue(double dXValue,double dYValue);
private:
	virtual double compute(double dXValue,double dYValue);
};
}
#endif // !defined(AFX_NEARESTNEIGHBOURINTERPOLATOR2D_H__B66CF9B5_666E_4B14_8DCF_D358B549AF3B__INCLUDED_)
