#ifndef _INCLUDE_DATAFILE2D_H
#define _INCLUDE_DATAFILE2D_H

#include "DataFile.h"

namespace ICLIB {

class DataFile2D : public DataFile {
public:
	DataFile2D();
	virtual ~DataFile2D();

	virtual void load(const std::string& filename) /*throw(Tango::DevFailed)*/;
	virtual void save(const std::string& filename) /*throw(Tango::DevFailed)*/;

	void getArrayValues(vector<double>& aray,int& xdim,int& ydim);
	void setArrayValues(const vector<double>& aray,int xdim,int ydim);
};

} // namespace

#endif // _INCLUDE_DATAFILE2D_H
