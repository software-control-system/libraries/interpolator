#ifndef _INCLUDE_DATAFILE_H
#define _INCLUDE_DATAFILE_H

#include <ParseException.h>
#include <FileNotFoundException.h>
#include <vector>
#include <map>
#include <fstream>

namespace ICLIB {

typedef std::vector< double > Values;
typedef std::map< std::string,double > ValueMap;

class DataFile {

public:
	typedef std::vector< std::pair< std::string,Values > > ValuesMap;

	virtual ~DataFile() {}

	void clear() { mValues.clear(); }
	virtual void load(const std::string& filename) /*throw(ParseException)*/ =0;
	virtual void save(const std::string& filename) /*throw(ParseException)*/ =0;

	const Values& getValues(const std::string& key) const /*throw(ParseException)*/;
	void setValues(const std::string& key,const Values& data) /*throw(ParseException)*/;

	virtual void getArrayValues(vector<double>& array,int& xdim,int& ydim) =0;
	virtual void setArrayValues(const vector<double>& array,int xdim,int ydim) =0;

protected:
	ValuesMap mValues;
	Values mEmpty;
	std::string mFilename;
};

} // namespace

#endif // _INCLUDE_DATAFILE_H
