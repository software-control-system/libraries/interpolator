// LowEnergyThetaBraggCorrectionReader.h: interface for the LowEnergyThetaBraggCorrectionReader class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOWENERGYTHETABRAGGCORRECTIONREADER_H__183923E3_5940_437C_BB9B_4BDF7C2815E6__INCLUDED_)
#define AFX_LOWENERGYTHETABRAGGCORRECTIONREADER_H__183923E3_5940_437C_BB9B_4BDF7C2815E6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FileReader.h"
#include <vector>
#include "IndexOutOfBoundException.h"
#include "NullPointerException.h"
#include "ParseException.h"
//using namespace std;
namespace Interpolator
{
///This will become useless !!!
///This class is a file reader for a the low energy theta bragg correction. 
class LowEnergyThetaBraggCorrectionReader : public FileReader  
{
public:
	LowEnergyThetaBraggCorrectionReader();
	LowEnergyThetaBraggCorrectionReader(const std::string& sFileName);
	LowEnergyThetaBraggCorrectionReader(const std::string& sFileName,long lNbColumns,long lNbLines);
	virtual ~LowEnergyThetaBraggCorrectionReader();

	virtual void Parse() ;// throw (ParseException);

	double* getEnergy()			const			;// throw (NullPointerException);
	double* getThetaDiff()		const			;// throw (NullPointerException);
	double* getFitDiff()		const			;// throw (NullPointerException);
	double* getFWHMResolution()	const			;// throw (NullPointerException);

	double getValue(int iColumn, int iIndex) const ;// throw (IndexOutOfBoundException);
	
	double getEnergyValue(int i)			const;
	double getThetaDiffValue(int i)			const;
	double getFitDiffValue(int i)			const;
	double getFWHMResolutionValue(int i)	const;

	std::string getEnergyName()			const;
	std::string getThetaDiffName()		const;
	std::string getFitDiffName()			const;
	std::string getFWHMResolutionName()	const;

private:
	double* _mEnergy;
	double* _mThetaDiff;
	double* _mFitDiff;
	double* _mFWHMResolution;

	std::string _sEnergyName;
	std::string _sThetaDiffName;
	std::string _sFitDiffName;
	std::string _sFWHMResolutionName;

};
}
#endif // !defined(AFX_LOWENERGYTHETABRAGGCORRECTIONREADER_H__183923E3_5940_437C_BB9B_4BDF7C2815E6__INCLUDED_)





