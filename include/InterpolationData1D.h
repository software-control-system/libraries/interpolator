// InterpolationData1D.h: interface for the InterpolationData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_InterpolationData1D_H__780CD822_1132_472E_B13A_FD205D1E026B__INCLUDED_)
#define AFX_InterpolationData1D_H__780CD822_1132_472E_B13A_FD205D1E026B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>
//using namespace std;
#include "IndexOutOfBoundException.h"
#include "NullPointerException.h"
#include "Interpolator.h"

namespace Interpolator
{

///This class stores 1D data for a 1D interpolator
class InterpolationData1D  
{
public:
	InterpolationData1D();
	InterpolationData1D(std::string sXName, std::string sYName, long lNbData,double* mXValues, double* mYValues);
	virtual ~InterpolationData1D();

	std::string		getXName() const;
	std::string		getYName() const;
	double			getXValue(int i) const ;// throw (IndexOutOfBoundException,NullPointerException);
	double			getYValue(int i) const ;// throw (IndexOutOfBoundException,NullPointerException);
	
	double*			getXValues() const ;// throw (NullPointerException);
	double*			getYValues() const ;// throw (NullPointerException);

	void			setXValue(int i,double dNewValue) ;// throw (IndexOutOfBoundException,NullPointerException);
	void			setYValue(int i,double dNewValue) ;// throw (IndexOutOfBoundException,NullPointerException);

	void			setValues(long lNbData,double* dNewXValues,double* dNewYValues);
	long			getNbData() const;
	int				checkDataValidity(long lNbData,double* dNewXValues);
	void			checkSize(long lNbData);
	void			DisplayData();
private:	
	void			setXValues(double* dNewValues);
	void			setYValues(double* dNewValues);
	void			setXValuesInInverseOrder(double* dNewValues);
	void			setYValuesInInverseOrder(double* dNewValues);
	
	std::string		_sXName;
	std::string		_sYName;
	long			_lNbData;
	double*			_mXValues;
	double*			_mYValues;
};
}
#endif // !defined(AFX_InterpolationData1D_H__780CD822_1132_472E_B13A_FD205D1E026B__INCLUDED_)
