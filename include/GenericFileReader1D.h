// GenericFileReader1D1D.h: interface for the GenericFileReader1D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GenericFileReader1D_H__F01D8B25_4EB3_4DF6_9D97_3F4AE2F94FAF__INCLUDED_)
#define AFX_GenericFileReader1D_H__F01D8B25_4EB3_4DF6_9D97_3F4AE2F94FAF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FileReader.h"
#include <vector>
#include "IndexOutOfBoundException.h"
#include "NullPointerException.h"
#include "ParseException.h"
//using namespace std;


namespace Interpolator
{
///This class is a generic file reader for file with data sorted by columns
class GenericFileReader1D : public FileReader  
{
public:
	GenericFileReader1D();
	virtual ~GenericFileReader1D();

	GenericFileReader1D(const std::string& sFileName);
	GenericFileReader1D(const std::string& sFileName,long lNbColumns,long lNbLines);

	virtual void Parse(); // throw (ParseException);

	double* getColumn(int iColumnNumber)				const;	// throw (IndexOutOfBoundException,NullPointerException);
	double getValue(int iColumnNumber, int iLineNumber)	const;	// throw (IndexOutOfBoundException,NullPointerException);
	
	std::string getColumnName(int iColumnNumber)		const;	// throw (IndexOutOfBoundException,NullPointerException);
	void displayAll();

private:
	vector<double*> *		_mColumnsVector;
	vector<std::string>*	_mColumnsNameVector;
};

}
#endif // !defined(AFX_GenericFileReader1D_H__F01D8B25_4EB3_4DF6_9D97_3F4AE2F94FAF__INCLUDED_)
