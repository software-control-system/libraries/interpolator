#ifndef _INCLUDE_DATAFILE1D_H
#define _INCLUDE_DATAFILE1D_H

#include "DataFile.h"

namespace ICLIB {

class DataFile1D : public DataFile {
public:
	DataFile1D();
	virtual ~DataFile1D();

	virtual void load(const std::string& filename) /*throw(Tango::DevFailed)*/;
	virtual void save(const std::string& filename) /*throw(Tango::DevFailed)*/;

	void getArrayValues(vector<double>& aray,int& xdim,int& ydim);
	void setArrayValues(const vector<double>& aray,int xdim,int ydim);
};

} // namespace

#endif // _INCLUDE_DATAFILE1D_H
