// AkimaInterpolator1D.h: interface for the AkimaInterpolator1D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AKIMAInterpolator1D_H__FB4EF769_FD9B_4764_8D0C_6A9ECEEF4509__INCLUDED_)
#define AFX_AKIMAInterpolator1D_H__FB4EF769_FD9B_4764_8D0C_6A9ECEEF4509__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Interpolator1D.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <string>

//using namespace std;

namespace Interpolator
{

///This class performs an Akima interpolation
class AkimaInterpolator1D : public Interpolator1D  
{
public:
	AkimaInterpolator1D();
	virtual ~AkimaInterpolator1D();

	AkimaInterpolator1D(	
				std::string sName, 
				std::string sDescription, 
				InterpolationData1D* mInterpolationData);

	virtual double getInterpolatedValue(double dValue);
	virtual void updateInterpolator();
};
}
#endif // !defined(AFX_AKIMAInterpolator1D_H__FB4EF769_FD9B_4764_8D0C_6A9ECEEF4509__INCLUDED_)
