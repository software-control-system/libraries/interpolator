// Table.h: interface for the Table class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABLE_H__90B89FDD_8A1C_4D06_A6DB_D982FFB6DBA8__INCLUDED_)
#define AFX_TABLE_H__90B89FDD_8A1C_4D06_A6DB_D982FFB6DBA8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComputingObject.h"
#include <string>
//using namespace std;

#include "Interpolator.h"
namespace Interpolator
{
///This class is the main interface for a Table object
///The values used to interpolate need to be increasingly ordered !!!
class Table : public ComputingObject  
{
public:
	Table();
	Table(std::string sName,std::string sDescription,std::string InterpolationType,std::string sFilePath="");
	virtual ~Table();
	std::string	getName() const;
	std::string getDescription() const;
	std::string getInterpolationType() const;
	std::string getFilePath() const;
	
//	virtual double computeValue(double dXValue, double dYvalue)=0;
//	virtual double computeValue(double dValue)=0;
	virtual double computeValue()=0;

	virtual void				printInfos()=0;

	

private:
	std::string				_sName;
	std::string				_sDescription;
	std::string				_sInterpolationType;
	std::string				_sFilePath;



};
}
#endif // !defined(AFX_TABLE_H__90B89FDD_8A1C_4D06_A6DB_D982FFB6DBA8__INCLUDED_)

	
