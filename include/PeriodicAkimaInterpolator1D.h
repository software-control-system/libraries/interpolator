// PeriodicAkimaInterpolator1D.h: interface for the PeriodicAkimaInterpolator1D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PERIODICAKIMAInterpolator1D_H__FB4EF769_FD9B_4764_8D0C_6A9ECEEF4509__INCLUDED_)
#define AFX_PERIODICAKIMAInterpolator1D_H__FB4EF769_FD9B_4764_8D0C_6A9ECEEF4509__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Interpolator1D.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

namespace Interpolator
{
///This class performs a Periodic Akima interpolation
class PeriodicAkimaInterpolator1D : public Interpolator1D  
{
public:
	PeriodicAkimaInterpolator1D();
	virtual ~PeriodicAkimaInterpolator1D();

	PeriodicAkimaInterpolator1D(	
				std::string sName, 
				std::string sDescription, 
				InterpolationData1D* mInterpolationData);

	virtual double getInterpolatedValue(double dValue);
	virtual void updateInterpolator();

/*
private:
	gsl_interp_accel *acc;
	gsl_spline *spline;
*/
};
}
#endif // !defined(AFX_PERIODICAKIMAInterpolator1D_H__FB4EF769_FD9B_4764_8D0C_6A9ECEEF4509__INCLUDED_)
