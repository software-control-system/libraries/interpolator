// FourNearestNeighboursMeanInterpolator2D.h: interface for the FourNearestNeighboursMeanInterpolator2D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FOURNEARESTNEIGHBOURSMEANINTERPOLATOR2D_H__557E6450_2A9A_4C96_959F_C36356FFF0E8__INCLUDED_)
#define AFX_FOURNEARESTNEIGHBOURSMEANINTERPOLATOR2D_H__557E6450_2A9A_4C96_959F_C36356FFF0E8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "NearestNeighbourInterpolator2D.h"

namespace Interpolator
{
///This class performs a 4 nearest neighbour mean interpolation
class FourNearestNeighboursMeanInterpolator2D : public NearestNeighbourInterpolator2D  
{
public:
	FourNearestNeighboursMeanInterpolator2D();
	virtual ~FourNearestNeighboursMeanInterpolator2D();

	FourNearestNeighboursMeanInterpolator2D(
					std::string sName, 
					std::string sDescription,
					InterpolationData2D* mInterpolationData);

	virtual double getInterpolatedValue(double dXValue,double dYValue);
private:
	virtual double compute(double dXValue,double dYValue);
};
}
#endif // !defined(AFX_FOURNEARESTNEIGHBOURSMEANINTERPOLATOR2D_H__557E6450_2A9A_4C96_959F_C36356FFF0E8__INCLUDED_)
