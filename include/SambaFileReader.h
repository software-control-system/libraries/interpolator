// SambaFileReader.h: interface for the SambaFileReader class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SAMBAFILEREADER_H__3CD964AC_69BE_40F9_ABB6_05C5D8FA401F__INCLUDED_)
#define AFX_SAMBAFILEREADER_H__3CD964AC_69BE_40F9_ABB6_05C5D8FA401F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FileReader.h"
#include <vector>

#include "IndexOutOfBoundException.h"
#include "NullPointerException.h"
#include "ParseException.h"
//using namespace std;
namespace Interpolator
{
///This will become useless !!!
///This class is a file reader for a the Samba line.
class SambaFileReader : public FileReader  
{
public:
	SambaFileReader();
	SambaFileReader(const std::string& sFileName);
	SambaFileReader(const std::string& sFileName,long lNbColumns,long lNbLines);

	virtual ~SambaFileReader();
	virtual void Parse() ;// throw (ParseException);

	double* getThetaEncoder()	const			;// throw (NullPointerException);
	double* getThetaReal()		const			;// throw (NullPointerException);
	double* getThetaDiff()		const			;// throw (NullPointerException);

	double getXValue(int i)	const				;// throw (IndexOutOfBoundException);
	double getYValue(int i)	const				;// throw (IndexOutOfBoundException);

	double getThetaEncoderValue(int i)	const	;// throw (IndexOutOfBoundException);
	double getThetaRealValue(int i)		const	;// throw (IndexOutOfBoundException);
	
private:

	double* _mThetaEncoder;
	double* _mThetaReal;
	double* _mThetaDiff;

};
}
#endif // !defined(AFX_SAMBAFILEREADER_H__3CD964AC_69BE_40F9_ABB6_05C5D8FA401F__INCLUDED_)
