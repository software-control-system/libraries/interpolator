// FileReader.h: interface for the FileReader class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILEREADER_H__FBE90E13_CC25_465A_9449_79A971DF5770__INCLUDED_)
#define AFX_FILEREADER_H__FBE90E13_CC25_465A_9449_79A971DF5770__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string> 
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>

#include "FileNotFoundException.h"
#include "ParseException.h"
#include "Interpolator.h"
//using namespace std;

namespace Interpolator
{

///This class provides basics tools to read files\n
///It is mainly used for the correcting and coupling files\n
///Thanks to this abstract class, it is possible to define a specific file reader for each scientific group\n
///The only things to do is to implement the Parse method.
class FileReader  
{
public:
	FileReader();
	FileReader(const std::string& sFileName);									// throw (FileNotFoundException);
	FileReader(const std::string& sFileName,long lNbVariables,long lNbDatas);	// throw (FileNotFoundException);
	virtual ~FileReader();
	
	bool isReadable();
	
	double		extractDouble();
	std::string	extractString() ;
	int			extractInt();
	std::string	extractLine();
	
	long	extractFileSize();		// throw (FileNotFoundException);	
	long	extractNbFileLines();	// throw (FileNotFoundException);
	long	extractBufferSize();
//	long	extractNbData();
//	long	extractNbVariables();

	long	getFileSize()		const;
	long	getNbFileLines()	const;
	long	getBufferSize()		const;
	long	getNbVariables()	const;
	long	getNbData()		const;
	
	void	setFileSize		(long lFileSize);	
	void	setNbFileLines	(long lNbFileLines);
	void	setBufferSize	(long lBufferSize);
	void	setNbVariables	(long lNbVariables);
	void	setNbData		(long lNbData);

	void	copyFileToBuffer(); // throw (FileNotFoundException);
	
	virtual void Parse()=0; // throw (ParseException)=0;
	
	void extractHeader();

private:
	std::string			_sFileName;
	
	long				_lNbVariables;
	long				_lNbData;

	std::stringstream	_mFileBuffer;
	
	long				_lBufferSize;	
	long				_lFileSize;
	long				_lNbFileLines;

};
}
#endif // !defined(AFX_FILEREADER_H__FBE90E13_CC25_465A_9449_79A971DF5770__INCLUDED_)
