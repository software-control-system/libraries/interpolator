// Interpolator2D.h: interface for the Interpolator2D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INTERPOLATOR2D_H__7B57732B_DAA6_4074_A629_D63E6AE8C451__INCLUDED_)
#define AFX_INTERPOLATOR2D_H__7B57732B_DAA6_4074_A629_D63E6AE8C451__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Interpolator.h"
#include "InterpolationData2D.h"
#include <string>

//using namespace std;
namespace Interpolator
{
///This class provide an interface for a 2D interpolator
class Interpolator2D : public Interpolator 
{
public:

	Interpolator2D();

	Interpolator2D(	std::string sName, 
					std::string sDescription,
					std::string sInterpolationType,
					InterpolationData2D* mInterpolationData);
	
	virtual ~Interpolator2D();
	virtual double					getInterpolatedValue(double dXValue,double dYValue)=0;
	virtual InterpolationData2D*	getInterpolatedData();

	void findIndexes(double dXValue,double dYValue);
	int findXIndex(double dXValue);
	int findYIndex(double dYValue);
	virtual double compute(double dXValue,double dYValue)=0;

protected:
	InterpolationData2D*	_mInterpolationData;

private:

//	string					_sName;
//	string					_sDescription;
//	string					_sInterpolationType;

};
}
#endif // !defined(AFX_INTERPOLATOR2D_H__7B57732B_DAA6_4074_A629_D63E6AE8C451__INCLUDED_)
