// Table2D.h: interface for the Table2D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TABLE2D_H__45AF9FF5_F7DA_40AB_9C1E_146EACDA440F__INCLUDED_)
#define AFX_TABLE2D_H__45AF9FF5_F7DA_40AB_9C1E_146EACDA440F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "Table.h"

#include "ComputingObject.h"

#include <string>
//using namespace std;
#include "Table.h"
#include "Variable.h"
#include "InterpolationData2D.h"
#include "Interpolator2D.h"
#include "GenericFileReader2D.h"
namespace Interpolator
{
///This class is used to manage a 2D table.
///If you use the constructor with data (double*) it is necessary to delete the objects pointed.
///Indeed during the creation of the table, the data are copied in an internal structure.
class Table2D : public Table 
{
public:
//	Table2D();
	virtual ~Table2D();

	Table2D(std::string			sName,
			std::string			sDescription,
			std::string			sInterpolationType,
			std::string			sFilePath);

	Table2D(std::string			sName,
			std::string			sDescription,	
			std::string			sInterpolationType,
			std::string			sXName,
			std::string			sYName,
			std::string			sZName,
			long				lNbDataX,
			long				lNbDataY,
			double*				mXValues,
			double*				mYValues,
			double*				mZValues);


	double computeValue();
	double computeValue(double dXValue, double dYvalue);
	
	Interpolator2D*			getInterpolator() const;	
	InterpolationData2D*	getInterpolationData() const;
	
	virtual void printInfos();

	void setXValue(int i,double dNewValue);
	void setYValue(int i,double dNewValue);
	void setZValue(int i,int j,double dNewValue);	
	void setValues(long lNbXData,long lNbYData,double* dNewXValues,double* dNewYValues,double* dNewZValues);	


	double getXValue(int i) const ;// throw (IndexOutOfBoundException,NullPointerException);
	double getYValue(int i) const ;// throw (IndexOutOfBoundException,NullPointerException);
	double getZValue(int i,int j) const ;// throw (IndexOutOfBoundException,NullPointerException);

	double* getXValues() const ;// throw (NullPointerException);
	double* getYValues() const ;// throw (NullPointerException);
	double* getZValues() const ;// throw (NullPointerException);

	long getNbXData() const;
	long getNbYData() const;
	long getNbZData() const;


private :

	void initializeInterpolator();

	Interpolator2D*			_mInterpolator;
	InterpolationData2D*	_mData;

};
}
#endif // !defined(AFX_TABLE2D_H__45AF9FF5_F7DA_40AB_9C1E_146EACDA440F__INCLUDED_)
